INSERT INTO MotifInterruption(CodeMotifInterruption,Libelle) VALUES
(1,'Vacances Noël'),
(2,'8 mai + Ascension 2013'),
(3,'Interruption fictive'),
(4,'Décalage pour modules de formation discontinus'),
(5,'Alternance BAC+5'),
(6,'Fermeture du centre'),
(7,'Interruption');
