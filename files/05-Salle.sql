INSERT INTO osmose_room(
CodeSalle,Libelle,Capacite,Lieu) VALUES
('0001','Salle 1',6,4),
('0002','Salle 2',18,4),
('001','Salle 001',28,1),
('002','Salle 002',26,1),
('003','Salle 003',10,1),
('1','Salle 0.1',24,2),
('100','Salle 100',1,6),
('1000','Salle 1000',1,6),
('10000','Salle HUB1',33,1),
('10001','Salle',15,6),
('101','Salle 101',29,1),
('102','Salle 102',28,1),
('104','Salle 104',21,1),
('105','Salle 105',20,1),
('2','Salle 0.2',24,2),
('200','Salle 200',1,6),
('201','Salle 201',28,1),
('202','Salle 202',28,1),
('204','Salle 204',21,1),
('205','Salle 205',21,1),
('206','Salle 206',21,1),
('207','Salle 207',25,1),
('26','Salle 2.6',21,2),
('27','Salle 2.7',28,2),
('28','Salle 2.8',21,2),
('3','Salle 1.3',31,2),
('300','Salle 300',1,6),
('303','Salle 303',2,1),
('304','Salle 304',15,1),
('4','Salle 1.4',24,2),
('400','Salle 400',1,6),
('5','Salle 1.5',18,2),
('500','Salle 500',1,6),
('600','Salle 600',1,6),
('700','Salle 700',1,6),
('800','Salle 800',1,6),
('888','Salle Piscine',11,2),
('889','Salle 2 ENIService',10,2),
('900','Salle 900',1,6),
('99','Salle JURY',3,7),
('999','Salle Niort',12,5),
('B300','Salle B300',3,1)
