
/****** Object:  Table [dbo].[Cours]    Script Date: 19/01/2016 21:44:57 ******/
/* SET ANSI_NULLS ON */

/* SET QUOTED_IDENTIFIER ON */

/* SET ANSI_PADDING OFF */

drop table if exists Cours;
CREATE TABLE Cours(
	`Debut` datetime(3) NOT NULL,
	`Fin` datetime(3) NOT NULL,
	`DureeReelleEnHeures` smallint NOT NULL,
	`CodePromotion` char(8) NULL,
	`IdCours` Char(36) NOT NULL,
	`PrixPublicAffecte` Double NOT NULL,
	`DateCreation` datetime(3) ,
	`DateModif` datetime(3) NULL,
	`IdModule` int NOT NULL,
	`LibelleCours` varchar(250) NOT NULL,
	`DureePrevueEnHeures` smallint NOT NULL,
	`DateAdefinir` Tinyint NOT NULL DEFAULT 0,
	`CodeSalle` char(5) NULL,
	`CodeFormateur` int NULL,
 CONSTRAINT `PK_Cours` PRIMARY KEY
(
	`IdCours` ASC
)
);

/* SET ANSI_PADDING OFF */

/****** Object:  Table [dbo].[Formateur]    Script Date: 19/01/2016 21:44:58 ******/
/* SET ANSI_NULLS ON */

/* SET QUOTED_IDENTIFIER ON */

/* SET ANSI_PADDING ON */

drop table if exists Formateur;
CREATE TABLE Formateur(
	`CodeFormateur` int AUTO_INCREMENT NOT NULL,
	`Nom` nvarchar(100) NULL,
	`Prenom` nvarchar(100) NULL,
	`IsExterne` Tinyint NOT NULL   DEFAULT 0,
	`UserName` varchar(50) NULL,
	`Mail` nvarchar(200) NULL,
 CONSTRAINT `PK_Formateur` PRIMARY KEY
(
	`CodeFormateur` ASC
)
);

/* SET ANSI_PADDING OFF */

/****** Object:  Table [dbo].[Formation]    Script Date: 19/01/2016 21:44:58 ******/
/* SET ANSI_NULLS ON */

/* SET QUOTED_IDENTIFIER ON */

/* SET ANSI_PADDING OFF */

drop table if exists Formation;
CREATE TABLE Formation(
	`CodeFormation` char(8) NOT NULL,
	`LibelleLong` varchar(200) NOT NULL,
	`LibelleCourt` varchar(50) NOT NULL,
	`DureeEnHeures` smallint NOT NULL,
	`TauxHoraire` Double NOT NULL,
	`DateCreation` datetime(3) ,
	`CodeTitre` char(8) NULL,
	`PrixPublicEnCours` Double NOT NULL,
	`HeuresCentre` smallint NULL,
	`HeuresStage` smallint NULL,
	`SemainesCentre` tinyint UNSIGNED NULL,
	`SemainesStage` tinyint UNSIGNED NULL,
	`DureeEnSemaines` tinyint UNSIGNED NOT NULL,
	`DateModif` datetime(3) NULL,
	`Archiver` Tinyint NOT NULL   DEFAULT 0,
	`ECFaPasser` int NULL,
 CONSTRAINT `PK_Formation` PRIMARY KEY
(
	`CodeFormation` ASC
)
);

/* SET ANSI_PADDING OFF */

/****** Object:  Table [dbo].[InterruptionParPromo]    Script Date: 19/01/2016 21:44:58 ******/
/* SET ANSI_NULLS ON */

/* SET QUOTED_IDENTIFIER ON */

/* SET ANSI_PADDING OFF */

drop table if exists InterruptionParPromo;
CREATE TABLE InterruptionParPromo(
	`CodeMotifInterruption` int NOT NULL,
	`CodePromotion` char(8) NOT NULL,
	`Debut` datetime(3) NOT NULL,
	`Fin` datetime(3) NOT NULL,
	`IdInterruption` int AUTO_INCREMENT NOT NULL,
PRIMARY KEY
(
	`IdInterruption` ASC
)
);

/* SET ANSI_PADDING OFF */

/****** Object:  Table [dbo].[Lieu]    Script Date: 19/01/2016 21:44:58 ******/
/* SET ANSI_NULLS ON */

/* SET QUOTED_IDENTIFIER ON */

drop table if exists Lieu;
CREATE TABLE Lieu(
	`CodeLieu` int AUTO_INCREMENT NOT NULL,
	`Libelle` nvarchar(50) NOT NULL,
 CONSTRAINT `PK_Lieu` PRIMARY KEY
(
	`CodeLieu` ASC
)
);

/****** Object:  Table [dbo].[Module]    Script Date: 19/01/2016 21:44:58 ******/
/* SET ANSI_NULLS ON */

/* SET QUOTED_IDENTIFIER ON */

/* SET ANSI_PADDING OFF */

drop table if exists Module;
CREATE TABLE Module(
	`Libelle` varchar(200) NOT NULL,
	`DureeEnHeures` smallint NOT NULL,
	`DateCreation` datetime(3) ,
	`DureeEnSemaines` tinyint UNSIGNED NOT NULL,
	`PrixPublicEnCours` Double NOT NULL,
	`LibelleCourt` varchar(20) NOT NULL,
	`IdModule` int AUTO_INCREMENT NOT NULL,
	`DateModif` datetime(3) NULL,
	`Archiver` Tinyint NOT NULL   DEFAULT 0,
	`Detail` Longtext NULL,
 CONSTRAINT `PK_Module` PRIMARY KEY
(
	`IdModule` ASC
)
);

GO
/* SET ANSI_PADDING OFF */

/****** Object:  Table [dbo].[MotifInterruption]    Script Date: 19/01/2016 21:44:58 ******/
/* SET ANSI_NULLS ON */

/* SET QUOTED_IDENTIFIER ON */

/* SET ANSI_PADDING OFF */

drop table if exists MotifInterruption;
CREATE TABLE MotifInterruption(
	`CodeMotifInterruption` int AUTO_INCREMENT NOT NULL,
	`Libelle` varchar(500) NOT NULL,
 CONSTRAINT `PK_MotifInterruption` PRIMARY KEY
(
	`CodeMotifInterruption` ASC
)
);

/* SET ANSI_PADDING OFF */

/****** Object:  Table [dbo].[PlanningIndividuelDetail]    Script Date: 19/01/2016 21:44:58 ******/
/* SET ANSI_NULLS ON */

/* SET QUOTED_IDENTIFIER ON */

drop table if exists PlanningIndividuelDetail;
CREATE TABLE PlanningIndividuelDetail(
	`CodePlanning` int NOT NULL,
	`IdCours` Char(36) NOT NULL,
	`PrixCoursDevis` Double NOT NULL,
	`PrixCoursPECDevis` Double NULL,
	`PrixCoursFinanceDevis` Double NULL,
	`Dispense` Tinyint NOT NULL,
	`Inscrit` Tinyint NOT NULL,
	`DebutCours` datetime(3) NULL,
	`FinCours` datetime(3) NULL,
	`HeuresReellesCours` smallint NULL,
	`FacturationPlanning` Tinyint NOT NULL   DEFAULT 0,
 CONSTRAINT `PK_PlanningIndividuelDetail` PRIMARY KEY
(
	`CodePlanning` ASC,
	`IdCours` ASC
)
);

/****** Object:  Table [dbo].[PlanningIndividuelFormation]    Script Date: 19/01/2016 21:44:58 ******/
/* SET ANSI_NULLS ON */

/* SET QUOTED_IDENTIFIER ON */

/* SET ANSI_PADDING ON */

drop table if exists PlanningIndividuelFormation;
CREATE TABLE PlanningIndividuelFormation(
	`CodePlanning` int AUTO_INCREMENT NOT NULL,
	`CodeStagiaire` int NOT NULL,
	`DateInscription` datetime(3) NULL,
	`DateCreation` datetime(3) ,
	`CodeTypeProfil` int NOT NULL,
	`CodeFormation` char(8) NULL,
	`CodePromotion` char(8) NULL,
	`DateModif` datetime(3) NULL,
	`NumLien` int NULL,
	`FacturationPlanning` Tinyint NOT NULL   DEFAULT 0,
	`CodeReac` int NULL,
 CONSTRAINT `PK_PlanningIndividuelFormation` PRIMARY KEY
(
	`CodePlanning` ASC
)
);

/* SET ANSI_PADDING OFF */

/****** Object:  Table [dbo].[Promotion]    Script Date: 19/01/2016 21:44:58 ******/
/* SET ANSI_NULLS ON */

/* SET QUOTED_IDENTIFIER ON */

/* SET ANSI_PADDING OFF */

drop table if exists Promotion;
CREATE TABLE Promotion(
	`CodePromotion` char(8) NOT NULL,
	`Libelle` varchar(200) NOT NULL,
	`Debut` datetime(3) NOT NULL,
	`Fin` datetime(3) NOT NULL,
	`CodeFormation` char(8) NOT NULL,
	`PrixPublicAffecte` Double NOT NULL,
	`DateModif` datetime(3) NULL,
	`DateCreation` datetime(3),
	`PrixPECAffecte` Double NOT NULL,
	`PrixFinanceAffecte` Double NOT NULL,
 CONSTRAINT `PK_Promotion` PRIMARY KEY
(
	`CodePromotion` ASC
)
);

/* SET ANSI_PADDING OFF */

/****** Object:  Table [dbo].[Salle]    Script Date: 19/01/2016 21:44:58 ******/
/* SET ANSI_NULLS ON */

/* SET QUOTED_IDENTIFIER ON */

/* SET ANSI_PADDING OFF */

drop table if exists Salle;
CREATE TABLE Salle(
	`CodeSalle` varchar(5) NOT NULL,
	`Libelle` varchar(50) NOT NULL,
	`Capacite` int NULL,
	`Lieu` int NOT NULL DEFAULT 0,
 CONSTRAINT `PK_Salle` PRIMARY KEY
(
	`CodeSalle` ASC
)
);

/* SET ANSI_PADDING OFF */

