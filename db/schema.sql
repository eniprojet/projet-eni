-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Location` (
  `id_location` INT NOT NULL,
  `city` VARCHAR(150) NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `coordinates` POINT NOT NULL,
  PRIMARY KEY (`id_location`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Site`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Site` (
  `id_site` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `opening_hour` DATETIME NOT NULL,
  `closing_hour` DATETIME NOT NULL,
  `break_start` DATETIME NOT NULL,
  `break_end` DATETIME NOT NULL,
  `location_id` INT NOT NULL,
  PRIMARY KEY (`id_site`, `location_id`),
  INDEX `fk_Site_Location1_idx` (`location_id` ASC),
  CONSTRAINT `fk_Site_Location1`
    FOREIGN KEY (`location_id`)
    REFERENCES `mydb`.`Location` (`id_location`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`User` (
  `id_user` INT NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `role` ENUM('admin', 'extern', 'intern', 'superadmin') NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(64) NULL,
  `location_id` INT NOT NULL,
  PRIMARY KEY (`id_user`, `location_id`),
  INDEX `fk_User_Location1_idx` (`location_id` ASC),
  CONSTRAINT `fk_User_Location1`
    FOREIGN KEY (`location_id`)
    REFERENCES `mydb`.`Location` (`id_location`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Room`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Room` (
  `id_room` INT NOT NULL,
  `seats` INT NOT NULL,
  `computers` TEXT NULL,
  `projector` TINYINT(1) NOT NULL DEFAULT 0,
  `number` INT NOT NULL,
  `computers_models` VARCHAR(45) NULL,
  `computers_ram` VARCHAR(45) NULL,
  `computers_hdd1` VARCHAR(45) NULL,
  `computers_hdd2` VARCHAR(45) NULL,
  `computers_cpu` VARCHAR(45) NULL,
  `network` TINYINT(1) NOT NULL DEFAULT 0,
  `development` TINYINT(1) NOT NULL DEFAULT 0,
  `warranty_end` DATETIME NULL,
  `site_id` INT NOT NULL,
  PRIMARY KEY (`id_room`, `site_id`),
  INDEX `fk_Room_Site1_idx` (`site_id` ASC),
  CONSTRAINT `fk_Room_Site1`
    FOREIGN KEY (`site_id`)
    REFERENCES `mydb`.`Site` (`id_site`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Swarm` (
  `id_group` INT NOT NULL,
  `name` VARCHAR(15) NOT NULL,
  `supervisor_id` INT NOT NULL,
  PRIMARY KEY (`id_group`),
  INDEX `fk_Group_User1_idx` (`supervisor_id` ASC),
  CONSTRAINT `fk_Group_User1`
    FOREIGN KEY (`supervisor_id`)
    REFERENCES `mydb`.`User` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Slot`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Slot` (
  `id_slot` INT NOT NULL,
  `from` DATETIME NOT NULL,
  `to` DATETIME NOT NULL,
  `type` ENUM('ecf', 'preparation', 'cours') NOT NULL,
  `note` TEXT NULL,
  `room_id` INT NOT NULL,
  `trainer_id` INT NOT NULL,
  `group_id` INT NOT NULL,
  `site_id` INT NOT NULL,
  PRIMARY KEY (`id_slot`),
  INDEX `fk_Slot_Room1_idx` (`room_id` ASC),
  INDEX `fk_Slot_User1_idx` (`trainer_id` ASC),
  INDEX `fk_Slot_Group1_idx` (`group_id` ASC),
  INDEX `fk_Slot_Site1_idx` (`site_id` ASC),
  CONSTRAINT `fk_Slot_Room1`
    FOREIGN KEY (`room_id`)
    REFERENCES `mydb`.`Room` (`id_room`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Slot_User1`
    FOREIGN KEY (`trainer_id`)
    REFERENCES `mydb`.`User` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Slot_Group1`
    FOREIGN KEY (`group_id`)
    REFERENCES `mydb`.`Group` (`id_group`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Slot_Site1`
    FOREIGN KEY (`site_id`)
    REFERENCES `mydb`.`Site` (`id_site`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
