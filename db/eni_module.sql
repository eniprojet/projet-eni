-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Jeu 20 Octobre 2016 à 10:40
-- Version du serveur :  5.6.28
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `osmose`
--

-- --------------------------------------------------------

--
-- Structure de la table `eni_module`
--

CREATE TABLE `eni_module` (
  `id` int(11) NOT NULL,
  `libelle` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `prix_public_en_cours` double NOT NULL,
  `libelle_court` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `archiver` tinyint(1) NOT NULL,
  `detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `duree_en_heures` int(11) NOT NULL,
  `date_creation` datetime NOT NULL,
  `duree_en_semaines` smallint(6) NOT NULL,
  `date_modif` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `eni_module`
--

INSERT INTO `eni_module` (`id`, `libelle`, `prix_public_en_cours`, `libelle_court`, `archiver`, `detail`, `duree_en_heures`, `date_creation`, `duree_en_semaines`, `date_modif`) VALUES
(1, 'Logique de programmation', 0, 'INI-1', 1, 'Detail du module', 35, '2011-03-07 00:00:00', 1, '0000-00-00 00:00:00'),
(2, 'Environnement de développement graphique', 0, 'INI-2', 1, 'Detail du module', 35, '2011-03-07 00:00:00', 1, '0000-00-00 00:00:00'),
(3, 'SQL', 0, 'INI-3', 1, 'Detail du module', 35, '2011-03-07 00:00:00', 1, '0000-00-00 00:00:00'),
(4, 'Programmation objet sous Windows avec VB .Net', 0, 'VB-1', 1, 'Detail du module', 105, '2011-03-07 00:00:00', 3, '0000-00-00 00:00:00'),
(5, 'Accès à une base de données relationnelles avec VB .Net', 0, 'VB-2', 1, 'Detail du module', 70, '2011-03-07 00:00:00', 2, '0000-00-00 00:00:00'),
(6, 'Projet : Réalisation d\'une application en mode client/serveur', 0, 'VB-3', 1, 'Detail du module', 70, '2011-03-07 00:00:00', 2, '0000-00-00 00:00:00'),
(7, 'Cahier des charges', 0, 'CDC', 1, 'Detail du module', 35, '2011-03-07 00:00:00', 1, '0000-00-00 00:00:00'),
(8, 'Modélisation avec CDM', 0, 'CDM', 1, 'Detail du module', 35, '2011-03-07 00:00:00', 1, '0000-00-00 00:00:00'),
(9, 'PL-SQL', 0, 'PL-SQL', 1, 'Detail du module', 35, '2011-03-07 00:00:00', 1, '0000-00-00 00:00:00'),
(10, 'Java-J2SE', 0, 'JAVA-J2SE', 1, 'Detail du module', 105, '2011-03-07 00:00:00', 3, '0000-00-00 00:00:00'),
(11, 'RUP-UML', 0, 'RUP-UML', 1, 'Detail du module', 35, '2011-03-07 00:00:00', 1, '0000-00-00 00:00:00'),
(13, 'Projet RUP-UML', 0, 'PROJET 2-1', 1, 'Detail du module', 70, '2011-03-07 00:00:00', 2, '0000-00-00 00:00:00'),
(14, 'Java J2E', 0, 'J2E', 1, 'Detail du module', 70, '2011-03-07 00:00:00', 2, '0000-00-00 00:00:00'),
(15, 'Projet java J2E', 0, 'PROJET 2-2', 1, 'Detail du module', 70, '2011-03-07 00:00:00', 2, '0000-00-00 00:00:00'),
(16, '3ème semaine de projet Java J2E pour les DL', 0, 'PROJET 2-3', 1, 'Detail du module', 35, '2011-03-07 00:00:00', 1, '0000-00-00 00:00:00'),
(17, 'Stage Développeur Logiciel', 0, 'STAGE DL', 1, 'Detail du module', 280, '2011-03-07 00:00:00', 8, '0000-00-00 00:00:00'),
(18, 'Algorithme', 0, 'ALGO', 1, 'Detail du module', 35, '2011-05-02 00:00:00', 1, '0000-00-00 00:00:00'),
(19, 'Programmation en mode console', 0, 'INIPROG', 0, 'Detail du module', 35, '2011-05-02 00:00:00', 1, '0000-00-00 00:00:00'),
(20, 'Le langage de requête SQL', 0, 'SQL', 0, 'Detail du module', 35, '2011-05-02 00:00:00', 1, '0000-00-00 00:00:00'),
(21, 'Procédures stockées avec PL-SQL', 0, 'PL-SQL', 0, 'Detail du module', 35, '2011-05-02 00:00:00', 1, '0000-00-00 00:00:00'),
(22, 'Programmation orientée objet', 0, 'POO', 0, 'Detail du module', 70, '2011-05-02 00:00:00', 2, '0000-00-00 00:00:00'),
(23, 'Programmation objet avec VB.Net partie 1', 0, 'VB-1', 1, 'Detail du module', 70, '2011-05-02 00:00:00', 2, '0000-00-00 00:00:00'),
(24, 'Programmation objet avec VB.Net partie 2', 0, 'VB-2', 1, 'Detail du module', 70, '2011-05-02 00:00:00', 2, '0000-00-00 00:00:00'),
(25, 'Projet : Réalisation d\'une application en mode client/serveur', 0, 'PROJET-VB', 1, 'Detail du module', 70, '2011-05-02 00:00:00', 2, '0000-00-00 00:00:00'),
(26, 'Cours Conception et analyse', 0, 'CONC-APP-1', 1, 'Detail du module', 70, '2011-05-02 00:00:00', 2, '0000-00-00 00:00:00'),
(27, 'TP conception et analyse', 0, 'CONC-APP-2', 1, 'Detail du module', 35, '2011-05-02 00:00:00', 1, '0000-00-00 00:00:00'),
(28, 'JAVA J2E partie 1', 0, 'JAVA-1', 1, 'Detail du module', 70, '2011-05-02 00:00:00', 2, '0000-00-00 00:00:00'),
(29, 'JAVA J2E partie 2', 0, 'JAVA-2', 1, 'Detail du module', 70, '2011-05-02 00:00:00', 2, '0000-00-00 00:00:00'),
(30, 'PHP', 0, 'PHP', 0, 'Detail du module', 70, '2011-05-02 00:00:00', 2, '0000-00-00 00:00:00'),
(31, 'Projet : "Réalisation d\'une application internet/intranet"', 0, 'PROJ-JAVA-1', 0, 'Detail du module', 70, '2011-05-02 00:00:00', 2, '0000-00-00 00:00:00'),
(32, 'Projet JAVA ou PHP partie 2', 0, 'PROJ-JAVA-2', 1, 'Detail du module', 35, '2011-05-02 00:00:00', 1, '0000-00-00 00:00:00'),
(33, 'Stage Développeur Logiciel', 0, 'STAGE-DL', 0, 'Detail du module', 280, '2011-05-02 00:00:00', 8, '0000-00-00 00:00:00'),
(34, 'Utiliser les frameworks pour le développement avec Java EE', 0, 'J2EAV', 0, 'Detail du module', 70, '2011-05-03 00:00:00', 2, '0000-00-00 00:00:00'),
(35, 'Architectures applicatives', 0, 'ARCH-APP', 0, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(36, 'Conduite de projet', 0, 'CP-COURS', 0, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(37, 'Cours MS-Project + 2j ours de conduite de projet', 0, 'MS-PROJ', 1, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(38, 'Projet "Conduite de projet"', 0, 'CP-PROJ', 1, 'Detail du module', 70, '2011-05-03 00:00:00', 2, '0000-00-00 00:00:00'),
(39, 'Dernière semaine de CDI', 0, 'CDI-EVAL', 0, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(40, 'Stage CDI', 0, 'STAGE-CDI', 1, 'Detail du module', 350, '2011-05-03 00:00:00', 10, '0000-00-00 00:00:00'),
(41, 'Assistance bureautique 1ère semaine', 0, 'BUR-1', 1, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(42, 'Assistance bureautique 2ème semaine', 0, 'BUR-2', 1, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(43, 'Architecture PC', 0, 'ARCHI-PC', 1, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(44, 'Base des réseaux', 0, 'IM-BDR', 0, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(45, 'Utilisation et administration d\'un système Windows Client', 0, 'IM-W7', 0, 'Detail du module', 70, '2011-05-03 00:00:00', 2, '0000-00-00 00:00:00'),
(46, 'Assistance aux utilisateurs de W7', 0, 'IM-W7-ASS', 1, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(47, 'Utilisation et administration de Linux', 0, 'IM-LINUX', 1, 'Detail du module', 70, '2011-05-03 00:00:00', 2, '0000-00-00 00:00:00'),
(48, 'TP administration linux pour les IM', 0, 'IM-LINUX-TP', 1, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(51, 'ITIL, gestion de parc et centre de support', 0, 'IM-ITIL', 0, 'Detail du module', 70, '2011-05-03 00:00:00', 2, '0000-00-00 00:00:00'),
(52, 'Les services réseaux avec Linux', 0, 'IM-RES-LINUX', 1, 'Detail du module', 70, '2011-05-03 00:00:00', 2, '0000-00-00 00:00:00'),
(53, 'TP services réseaux hétérogènes', 0, 'IM-RES-TP', 0, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(54, 'Messagerie', 0, 'IM-MESSAG', 0, 'Detail du module', 70, '2011-05-03 00:00:00', 2, '0000-00-00 00:00:00'),
(55, 'Utilisation et administration d\'une base de données', 0, 'IM-BD', 1, 'Detail du module', 70, '2011-05-03 00:00:00', 2, '0000-00-00 00:00:00'),
(56, 'TP et évaluations finales', 0, 'IM-EVAL', 0, 'Detail du module', 35, '2011-05-03 00:00:00', 1, '0000-00-00 00:00:00'),
(57, 'Stage Informaticien Micro', 0, 'IM-STAGE', 0, 'Detail du module', 280, '2011-05-03 00:00:00', 8, '0000-00-00 00:00:00'),
(58, 'Services Réseaux Windows Server 2008 partie 1', 0, 'IM-RES-W-1', 1, 'Detail du module', 105, '2011-06-08 00:00:00', 3, '0000-00-00 00:00:00'),
(59, 'Services Réseaux avec Windows Server 2008 partie 2', 0, 'IM-RES-W-2', 1, 'Detail du module', 35, '2011-06-08 00:00:00', 1, '0000-00-00 00:00:00'),
(60, 'TP réseaux Linux', 0, 'IM-RES-TPL', 1, 'Detail du module', 35, '2011-06-08 00:00:00', 1, '0000-00-00 00:00:00');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `eni_module`
--
ALTER TABLE `eni_module`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `eni_module`
--
ALTER TABLE `eni_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;