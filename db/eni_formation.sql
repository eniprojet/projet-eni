

CREATE TABLE `eni_formation` (
  `id` int(11) NOT NULL,
  `CodeFormation` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `libelleLong` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `LibelleCourt` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DureeEnHeures` smallint(6) NOT NULL,
  `TauxHoraire` double NOT NULL,
  `DateCreation` datetime NOT NULL,
  `CodeTitre` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `PrixPublicEnCours` double NOT NULL,
  `HeuresCentre` smallint(6) NOT NULL,
  `HeuresStage` smallint(6) NOT NULL,
  `SemainesCentre` int(11) NOT NULL,
  `SemainesStage` int(11) NOT NULL,
  `DureeEnSemaines` int(11) NOT NULL,
  `DateModif` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Archiver` tinyint(1) NOT NULL,
  `ECFaPasser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `eni_formation` (`id`, `CodeFormation`, `libelleLong`, `LibelleCourt`, `DureeEnHeures`, `TauxHoraire`, `DateCreation`, `CodeTitre`, `PrixPublicEnCours`, `HeuresCentre`, `HeuresStage`, `SemainesCentre`, `SemainesStage`, `DureeEnSemaines`, `DateModif`, `Archiver`, `ECFaPasser`) VALUES
(1, 'AL', 'Architecte Logiciel', 'ARCHITECTE LOGICIEL', 595, 0, '2016-10-14 00:00:00', 'AL', 0, 595, 0, 17, 0, 17, '2016-10-14 00:00:00', 0, 0),
(2, 'ASR2011 ', 'Administrateur Système Et Réseau', 'ASR', 770, 0, '2016-10-14 00:00:00', 'ASR     ', 0, 490, 280, 14, 8, 22, '2016-10-14 00:00:00', 0, 0),
(3, 'ASR-2012', 'Administrateur Système Et Réseau', 'ASR2012', 735, 0, '2016-10-14 00:00:00', 'ASR     ', 0, 455, 280, 13, 8, 21, '2016-10-14 00:00:00', 0, 0),
(4, 'ASR-AV  ', 'Asr Avant 2011', 'ASR AVANT', 2310, 0, '2016-10-14 00:00:00', 'ASR     ', 0, 490, 1820, 14, 52, 66, '2016-10-14 00:00:00', 0, 0),
(5, 'CDI2011 ', 'Concepteur Développeur Informatique', 'CDI FC NANTES', 1365, 5, '2016-10-14 00:00:00', 'CDI     ', 8680, 1085, 280, 31, 8, 39, '2016-10-14 00:00:00', 0, 0),
(6, 'CDI-8S  ', 'Cdi Sur 8 Semaines +10', 'CDI PARTIE FINALE', 2100, 0, '2016-10-14 00:00:00', 'CDI     ', 0, 280, 1820, 8, 52, 60, '2016-10-14 00:00:00', 0, 0),
(7, 'CDI-CP  ', 'Concepteur Développeur Informatique', 'CDI CP NANTES', 1050, 0, '2016-10-14 00:00:00', 'CDI     ', 0, 1050, 0, 30, 0, 30, '2016-10-14 00:00:00', 0, 0),
(8, 'DL AVANT', 'Dl Avant Septembre 2011', 'DL AVANT 09-2011', 2660, 0, '2016-10-14 00:00:00', 'DL      ', 0, 840, 1820, 24, 52, 76, '2016-10-14 00:00:00', 0, 0),
(9, 'DL-100  ', 'Développeur Logiciel Dl100 Et U46', 'DL-100-U46', 875, 0, '2016-10-14 00:00:00', 'DL      ', 0, 525, 350, 15, 10, 25, '2016-10-14 00:00:00', 0, 0),
(10, 'DL-101  ', 'Développeur Logiciel Dl101', 'DL-101', 840, 0, '2016-10-14 00:00:00', 'DL      ', 0, 560, 280, 16, 8, 24, '2016-10-14 00:00:00', 0, 0),
(11, 'DL-101CP', 'Dl 101 Pour Régularisation Cp', 'DL-101 REGUL CP', 35, 0, '2016-10-14 00:00:00', '', 0, 35, 0, 1, 0, 1, '2016-10-14 00:00:00', 0, 0),
(12, 'DL-102CP', 'Dl-102 Régularisation Cp', 'DL-102 REGUL CP', 35, 0, '2016-10-14 00:00:00', '', 0, 35, 0, 1, 0, 1, '2016-10-14 00:00:00', 0, 0),
(13, 'DL2010  ', 'Développeur Logiciel Avant 09/2011', 'DL', 1120, 0, '2016-10-14 00:00:00', 'DL      ', 6170, 840, 280, 24, 8, 32, '2016-10-14 00:00:00', 0, 0),
(14, 'DL2011  ', 'Développeur Logiciel', 'DL NANTES', 1155, 5, '2016-10-14 00:00:00', 'DL      ', 6990, 875, 280, 25, 8, 33, '2016-10-14 00:00:00', 0, 0),
(15, 'DL2014  ', 'Développeur Logiciel', 'DL RENNES', 1134, 0, '2016-10-14 00:00:00', 'DL      ', 0, 854, 280, 25, 8, 33, '2016-10-14 00:00:00', 0, 0),
(16, 'EISI    ', 'Expert En Informatique Et Système D\'information', 'EISI', 595, 0, '2016-10-14 00:00:00', 'EISI    ', 0, 595, 0, 17, 0, 17, '2016-10-14 00:00:00', 0, 0),
(17, 'ENI-S-01', 'Formation Eni Service', 'ENI SERVICE', 35, 0, '2016-10-14 00:00:00', '', 0, 35, 0, 1, 0, 1, '2016-10-14 00:00:00', 0, 0),
(18, 'EVAL    ', 'Présentation Projet Final', 'EVALUATION', 7, 0, '2016-10-14 00:00:00', '', 0, 7, 0, 1, 0, 1, '2016-10-14 00:00:00', 0, 0),
(19, 'FORM    ', 'Formation', 'FORMATION', 0, 0, '2016-10-14 00:00:00', '', 0, 0, 0, 0, 0, 0, '2016-10-14 00:00:00', 0, 0),
(20, 'GEOS    ', 'Geos', 'GEOS', 875, 0, '2016-10-14 00:00:00', 'TSRIT   ', 0, 875, 0, 25, 0, 25, '2016-10-14 00:00:00', 0, 0),
(21, 'IM2011  ', 'Informaticien Micro', 'IM', 1120, 0, '2016-10-14 00:00:00', 'TSSI    ', 6319, 840, 280, 24, 8, 32, '2016-10-14 00:00:00', 0, 0),
(22, 'IM2014  ', 'Technicien(Ne) Supérieur(E) De Support En Informatique', 'T2SI NANTES', 1120, 0, '2016-10-14 00:00:00', 'TSSI    ', 6720, 840, 280, 24, 8, 32, '2016-10-14 00:00:00', 0, 0),
(23, 'IM67    ', 'Im67', 'IM67', 630, 0, '2016-10-14 00:00:00', 'TSSI    ', 0, 350, 280, 10, 8, 18, '2016-10-14 00:00:00', 0, 0),
(24, 'IM68    ', 'Im68', 'IM68', 910, 0, '2016-10-14 00:00:00', 'TSSI    ', 0, 630, 280, 18, 8, 26, '2016-10-14 00:00:00', 0, 0),
(25, 'IM69-RCP', 'Im-69 Régularisation Cp', 'IM-69 REGUL CP', 70, 0, '2016-10-14 00:00:00', '', 0, 70, 0, 2, 0, 2, '2016-10-14 00:00:00', 0, 0),
(26, 'IMAVANT ', 'Im Avant 2011', 'IM AVANT', 3500, 0, '2016-10-14 00:00:00', 'TSSI    ', 0, 805, 2695, 23, 77, 100, '2016-10-14 00:00:00', 0, 0),
(27, 'LICENCE ', 'Licence Informatique En Partenariat Avec Le Cnam', 'LICENCE INFORMATIQUE', 455, 0, '2016-10-14 00:00:00', 'LICENCE ', 0, 455, 0, 13, 0, 13, '2016-10-14 00:00:00', 0, 0),
(28, 'M AVANT ', 'Spécialiste Systèmes Et Réseaux Avant Septembre 2011', 'M AVANT 09-2011', 3500, 0, '2016-10-14 00:00:00', 'TSRIT   ', 0, 840, 2660, 24, 76, 100, '2016-10-14 00:00:00', 0, 0),
(29, 'M+ASR   ', 'Administrateur Système Et Réseau', 'ASR NANTES', 1645, 0, '2016-10-14 00:00:00', 'ASR     ', 10430, 1085, 560, 31, 16, 47, '2016-10-14 00:00:00', 0, 0),
(30, 'MODULE  ', 'Module', 'MODULE', 350, 0, '2016-10-14 00:00:00', '', 0, 350, 0, 10, 0, 10, '2016-10-14 00:00:00', 0, 0),
(31, 'MS2I    ', 'Manager Des Systèmes D\'information Et D\'infrastructure', 'MS2I', 595, 0, '2016-10-14 00:00:00', 'MS2I    ', 0, 595, 0, 17, 0, 17, '2016-10-14 00:00:00', 0, 0),
(32, 'MS2I-AL ', 'Manager Des Systèmes D\'information Et D\'infrastructure', 'MS2I-AL', 595, 0, '2016-10-14 00:00:00', 'MS2I    ', 0, 595, 0, 17, 0, 17, '2016-10-14 00:00:00', 0, 0),
(33, 'MS2I-IN ', 'Manager Des Systèmes D\'information Et D\'infrastructure Option Infrastructure', 'MS2I-IN', 490, 0, '2016-10-14 00:00:00', 'MS2I    ', 0, 490, 0, 14, 0, 14, '2016-10-14 00:00:00', 0, 0),
(34, 'MS2I-SI ', 'Manager Des Systèmes D\'information Et D\'infrastructure - Option Systèmes D\'information', 'MS2I-SI', 490, 0, '2016-10-14 00:00:00', 'MS2I    ', 0, 490, 0, 14, 0, 14, '2016-10-14 00:00:00', 0, 0),
(35, 'NEO-SOFT', 'Neo-Soft', 'NEO-SOFT', 735, 0, '2016-10-14 00:00:00', 'AUCUN   ', 0, 735, 0, 21, 0, 21, '2016-10-14 00:00:00', 0, 0),
(36, 'NI-ASR  ', 'Administrateur Système Et Réseau', 'ASR NIORT', 1085, 0, '2016-10-14 00:00:00', 'ASR     ', 0, 1085, 0, 31, 0, 31, '2016-10-14 00:00:00', 0, 0),
(37, 'NI-CDICP', 'Concepteur Développeur Informatique À Niort', 'CDI CP NIORT', 1050, 0, '2016-10-14 00:00:00', 'CDI     ', 0, 1050, 0, 30, 0, 30, '2016-10-14 00:00:00', 0, 0),
(38, 'POE JAVA', 'Poe Java', 'POE JAVA', 385, 0, '2016-10-14 00:00:00', '', 0, 385, 0, 11, 0, 11, '2016-10-14 00:00:00', 0, 0),
(39, 'POE PHP ', 'Poe Développeur Php', 'POE DÉVELOPPEUR PHP', 385, 0, '2016-10-14 00:00:00', '', 0, 315, 70, 9, 2, 11, '2016-10-14 00:00:00', 0, 0),
(40, 'PO-TECHS', 'Poe Technicien Support Client De Proximité', 'POE TECHNICIEN SUPPORT', 315, 0, '2016-10-14 00:00:00', '', 0, 315, 0, 9, 0, 9, '2016-10-14 00:00:00', 0, 0),
(41, 'PRJJAV  ', 'Projet Java Régularisation Cp', 'PRJ JAV', 35, 0, '2016-10-14 00:00:00', '', 0, 35, 0, 1, 0, 1, '2016-10-14 00:00:00', 0, 0),
(42, 'PRJVB   ', 'Projet Vb Regularisation Cp', 'PRJVB', 35, 0, '2016-10-14 00:00:00', '', 0, 35, 0, 1, 0, 1, '2016-10-14 00:00:00', 0, 0),
(43, 'R-ASR   ', 'Administrateur Systèmes Et Réseaux', 'ASR RENNES', 1085, 0, '2016-10-14 00:00:00', 'ASR     ', 0, 1085, 0, 31, 0, 31, '2016-10-14 00:00:00', 0, 0),
(44, 'R-CDI   ', 'Concepteur Développeur Informatique', 'CDI CP RENNES', 1050, 0, '2016-10-14 00:00:00', 'CDI     ', 0, 1050, 0, 30, 0, 30, '2016-10-14 00:00:00', 0, 0),
(45, 'R-CDI-FC', 'Concepteur(Rice) Développeur(Se) Informatique', 'CDI FC RENNES', 1274, 0, '2016-10-14 00:00:00', 'CDI     ', 8680, 994, 280, 29, 8, 37, '2016-10-14 00:00:00', 0, 0),
(46, 'R-EVAL  ', 'Présentation Projet Final', 'R-EVALUATION', 7, 0, '2016-10-14 00:00:00', '', 0, 7, 0, 1, 0, 1, '2016-10-14 00:00:00', 0, 0),
(47, 'R-IM    ', 'Technicien(Ne) Supérieur(E) De Support En Informatique', 'T2SI RENNES', 1134, 0, '2016-10-14 00:00:00', 'TSSI    ', 0, 854, 280, 25, 8, 33, '2016-10-14 00:00:00', 0, 0),
(48, 'R-SSR   ', 'Technicien(Ne) Supérieur(E) En Réseaux Informatiques Et Télécommunications', 'TSRIT RENNES', 1134, 0, '2016-10-14 00:00:00', 'TSRIT   ', 6740, 854, 280, 25, 8, 33, '2016-10-14 00:00:00', 0, 0),
(49, 'SSR2011 ', 'Specialiste Systèmes Et Réseaux', 'SSR', 1120, 0, '2016-10-14 00:00:00', 'TSRIT   ', 6740, 840, 280, 24, 8, 32, '2016-10-14 00:00:00', 0, 0),
(50, 'SSR76   ', 'Ssr76', 'SSR76', 455, 0, '2016-10-14 00:00:00', 'TSRIT   ', 0, 175, 280, 5, 8, 13, '2016-10-14 00:00:00', 0, 0),
(51, 'SSR77   ', 'Ssr77', 'SSR77', 805, 0, '2016-10-14 00:00:00', 'TSRIT   ', 0, 525, 280, 15, 8, 23, '2016-10-14 00:00:00', 0, 0),
(52, 'SSR78   ', 'Ssr78', 'SSR78', 70, 0, '2016-10-14 00:00:00', '', 0, 70, 0, 2, 0, 2, '2016-10-14 00:00:00', 0, 0),
(53, 'SSR79   ', 'Ssr79', 'SSR79', 70, 0, '2016-10-14 00:00:00', '', 0, 70, 0, 2, 0, 2, '2016-10-14 00:00:00', 0, 0),
(54, 'TAI     ', 'Technicien(Ne) D\'assistance En Informatique', 'TAI', 420, 0, '2016-10-14 00:00:00', 'TAI     ', 0, 420, 0, 12, 0, 12, '2016-10-14 00:00:00', 0, 0),
(55, 'TSRIT   ', 'Technicien Supérieur En Réseaux Informatiques Et Télécommunications', 'TSRIT NANTES', 1015, 0, '2016-10-14 00:00:00', 'TSRIT   ', 6740, 735, 280, 21, 8, 29, '2016-10-14 00:00:00', 0, 0);