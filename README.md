# Projet ENI #
============================

Vous pouvez retrouver la liste des actions à réaliser lors de l'installation du projet Symfony.

Requis pour l'utilisation du projet Symfony

1. Composer
2. Apache / MySQL / PHP 5+ (Soit avec WAMP / MAMP / LAMP ou autre).

## Sommaire ##

1. Installation
2. Configuration
3. Commandes de base en ligne de commande pour l'utilisation de Symfony

## Installation ##

### WAMP / EasyPHP ou autre ###
Lorsque vous changez de projet ou que vous installez ce projet pour la première fois vous devez :
* Installer WAMP / EasyPHP ou tout autre package vous permettant d'avoir Apache / MySQL (Sachant que sur Windows, WAMP reste le plus simple et puis y'a LAMP pour Julien :D ).

Site du téléchargement de WAMP : [http://www.wampserver.com/](http://www.wampserver.com/)

La version doit être la V 2.5. Vous devez choisir entre la 32 et la 64 bits en fonction de votre config. Vous n'êtes pas obligés de vous inscrire, le lien de téléchargement est en jaune dégueulasse sous le formulaire :)

### SourceTree ###

Si comme moi vous n'aimez pas forcément les lignes de commandes, vous pouvez télécharger SourceTree directement en bas de page sur le site de BitBucket ([bitbucket.org](bitbucket.org)).

Une fois ceci fait il vous sera demandé de vous connecter avec votre compte BitBucket.

### Composer ###

Symfony fonctionne essentiellement avec Composer (disponible ici :https://getcomposer.org/). Composer est un gestionnaire de librairie, il permet de télécharger des librairies externes à partir d'un fichier json (composer.json dans notre projet Symfony, à la racine).
C'est dans ce fichier que nous allons indiquer à Symfony, quelles sont les librairies dont nous avons besoin pour notre projet. (Exemple FOSUserBundle pour la gestion des utilisateurs)

### Projet Symfony ###

Lorsque vous télécharger le projet Symfony pour la première fois depuis Bitbucket (et donc GIT), vous devez télécharger les vendors (librairies externes), sans ça votre projet ne fonctionnera pas.

Pour cela, vous devez taper la commande : **composer install** dans le dossier du projet.

Attention : **composer install** n'est à effectuer que lors de la PREMIERE installation du projet sur votre environnement de développement. Lorsqu'on rajoute une librairie vous devez faire **composer update**.

Lorsqu'on fait une mise à jour des librairie il peut arriver que le fichier parameters.yml (présent dans app/config/parameters.yml) soit réinitialisé, faites une sauvegarde de temps en temps, on sait jamais.

## Configuration ##

## Commandes de base en ligne de commande pour l'utilisation de Symfony ##

### Configuration de la base de données ###

Le fichier de configuration parameters.yml regroupe les paramètres de configuration comme la base de données, le serveur SMTP, ou tout autre paramètre qui doit être
utilisé dans l'application d'une manière récurrente.

Le fichier se situe dans le dossier : **app/config/parameters.yml**

Chaque configuration est locale, vous pouvez avoir le nom de base de données que vous voulez etc. Le fichier n'est pas envoyé sur GIT, il est présent dans le fichier .gitignore à la racine du projet Symfony.

### Doctrine ###

Création de la base de données (initialisation du projet) : **php app/console doctrine:create:database**
Remplissage de la base de données avec les tables issues des entités : **php app/console doctrine:schema:update --dump-sql** (ne lance pas la requête mais on peut voir les requêtes SQL générées) ou **--force** (lance les requêtes SQL en base de données.

La gestion des entités, et par extension des tables de la base de données se fait directement en ligne de commande, vous ne devez pas toucher à la base de données directement.

### Fixtures ###

Les fixtures permettent d'initialiser la base par défaut. Afin de s'assurer de ne pas rencontrer de conflits, il faut exécuter les 3 commandes suivantes :

php app/console doctrine:schema:drop --force --full-database

php app/console doctrine:schema:update --force

php app/console doctrine:fixtures:load

Pour créer une nouvelle entité vous devez faire :

1. **php app/console doctrine:generate:entity**
2. Spécifier le bundle et le nom de l'entité, exemple : OSMOSEUserBundle:User (OSMOSE : Nom du projet / UserBundle : Nom du bundle / Uer : Nom de l'entité).
3. Renseigner les champs (on tape le nom du champ, le type [string, integer, boolean, date, datetime etc], la valeur associé [taille du string par exemple, ah ah énorme blague).

Une fois que tout ça est rentré, tapez entrée sur un champ vide. On va vous poser une question "Voulez-vous créer un repository associé". Le repository permet de créer proprement
des requêtes SQL plus complexe qu'un simple select *. On peut les créer à posteriori, donc faites comme vous le sentez, mais si vous pensez qu'il faut par exemple créer une requête de type
"SELECT COUNT(ID) FROM MATABLE", dites oui. Symfony nous mâche le boulot, on va pas cracher dessus :)

### FOS UserBundle ###

Ajout d'un utilisateur : php app/console fos:user:create (puis remplissage des champs demandés)
Changement d'un mot de passe : php app/console fos:user:change-password
Attribuer un rôle à un utilisateur : php app/console fos:user:promote puis rajouter le role genre ROLE_ADMIN / ROLE_DIRECTION etc, en respectant cette casse là (sans importante mais par convention c'est mieux et FOS User utilise déjà ROLE_USER, ROLE_ADMIN etc.)
Enlever un rôle : php app/console fos:user:demote

**Information : L'utilisateur lambda a un rôle de ROLE_USER non visible en base de données à l'inverse des autres rôles.**