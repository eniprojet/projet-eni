FROM debian:stable

ENV PORT 8000
ENV APP_HOME /app

RUN apt-get update && apt-get install -y \
    php5 \
    php5-dev \
    php-pear \
    libsodium-dev \
    git \
    unzip \
    php5-mysql \
    mysql-client

# Install php-libsodium
RUN echo 'extension=libsodium.so' > /etc/php5/mods-available/libsodium.ini
RUN pecl install libsodium
RUN php5enmod libsodium

# Setup php.ini for Symfony
RUN echo 'date.timezone="Europe/Paris"' >> /etc/php5/cli/php.ini

RUN mkdir $APP_HOME
ADD . $APP_HOME

WORKDIR $APP_HOME

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

EXPOSE $PORT
