<?php

namespace Osmose\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Osmose\BackendBundle\Entity\Swarm;
use Osmose\BackendBundle\Form\SwarmType;

/**
 * Swarm controller.
 *
 * @Route("/swarm")
 */
class SwarmController extends Controller
{

    /**
     * Lists all Swarm entities.
     *
     * @Route("/", name="swarm")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Swarm')->findAll();

        return array(
            'controller' => 'swarms',
            'entities' => $entities,
            'title_page' => 'Liste des promotions'
        );
    }
    /**
     * Creates a new Swarm entity.
     *
     * @Route("/", name="swarm_create")
     * @Method("POST")
     * @Template("BackendBundle:Swarm:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Swarm();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', "La promotion a bien été créée.");
            return $this->redirect($this->generateUrl('swarm_show', array('id' => $entity->getId())));
        }

        return array(
            'controller' => 'swarms',
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Swarm entity.
     *
     * @param Swarm $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Swarm $entity)
    {
        $form = $this->createForm(new SwarmType(), $entity, array(
            'action' => $this->generateUrl('swarm_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Swarm entity.
     *
     * @Route("/new", name="swarm_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Swarm();
        $form   = $this->createCreateForm($entity);

        return array(
            'controller' => 'swarms',
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Swarm entity.
     *
     * @Route("/{id}", name="swarm_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Swarm')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "La promotion n'existe pas.");
            return $this->redirect($this->generateUrl('swarm'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'controller' => 'swarms',
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Swarm entity.
     *
     * @Route("/{id}/edit", name="swarm_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Swarm')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "La promotion n'existe pas.");
            return $this->redirect($this->generateUrl('swarm'));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'controller' => 'swarms',
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'title_page'    => "Modification de la promotion",
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Swarm entity.
    *
    * @param Swarm $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Swarm $entity)
    {
        $form = $this->createForm(new SwarmType(), $entity, array(
            'action' => $this->generateUrl('swarm_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing Swarm entity.
     *
     * @Route("/{id}", name="swarm_update")
     * @Method("PUT")
     * @Template("BackendBundle:Swarm:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Swarm')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "La promotion n'existe pas.");
            return $this->redirect($this->generateUrl('swarm'));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', "La promotion a bien été modifiée.");
            return $this->redirect($this->generateUrl('swarm_show', array('id' => $id)));
        }

        return array(
            'controller' => 'swarms',
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Swarm entity.
     *
     * @Route("/{id}", name="swarm_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Swarm')->find($id);

            if (!$entity) {
                $this->get('session')->getFlashBag()->add('error', "La promotion n'existe pas.");
                return $this->redirect($this->generateUrl('swarm'));
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('swarm'));
    }

    /**
     * Creates a form to delete a Swarm entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('swarm_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
