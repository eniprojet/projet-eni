<?php

namespace Osmose\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Osmose\BackendBundle\Entity\Site;
use Osmose\BackendBundle\Form\SiteType;

/**
 * Site controller.
 *
 * @Route("/site")
 */
class SiteController extends Controller
{

    /**
     * Lists all Site entities.
     *
     * @Route("/", name="site")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sites = $this->get('backend.localisation')->getAllSites();

        if(!$this->isGranted("ROLE_ADMIN"))
        {
            $sites = $this->get('backend.localisation')->getAllEnabledSites();
        }

        $entity = new Site();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if($request->isMethod('POST'))
        {
            if ($form->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add('info', "Le site a bien été créé.");
                return $this->redirect($this->generateUrl('site_show', array('id' => $entity->getId())));
            }
        }

        return array(
            'controller'    => 'sites',
            'title_page'    => "Liste des sites",
            'sites'         => $sites,
            'form'          => $form->createView(),
        );
    }
    /**
     * Creates a new Site entity.
     *
     * @Route("/", name="site_create")
     * @Method("POST")
     * @Template("BackendBundle:Site:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Site();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', "Le site a bien été créé.");
            return $this->redirect($this->generateUrl('site_show', array('id' => $entity->getId())));
        }

        return array(
            'entity'        => $entity,
            'form'          => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Site entity.
     *
     * @param Site $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Site $entity)
    {
        $form = $this->createForm(new SiteType(), $entity, array(
            'action' => $this->generateUrl('site_create'),
            'method' => 'POST',
        ));
        return $form;
    }

    /**
     * Displays a form to create a new Site entity.
     *
     * @Route("/new", name="site_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Site();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity'        => $entity,
            'controller'    => 'sites',
            'title_page'    => "Ajouter d'un nouveau site",
            'form'          => $form->createView(),
        );
    }

    /**
     * Finds and displays a Site entity.
     *
     * @Route("/{id}", name="site_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Site')->find($id);
        $countRooms = $this->get('backend.room')->countRooms($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "Le site n'existe pas.");
            return $this->redirect($this->generateUrl('site'));
        }

        return array(
            'entity'        => $entity,
            'controller'    => 'sites',
            'countRooms'    => $countRooms
        );
    }

    /**
     * Edits an existing Site entity.
     *
     * @Route("/edit/{id}", name="site_edit")
     * @Template("BackendBundle:Site:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Site')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "Le site n'existe pas.");
            return $this->redirect($this->generateUrl('site'));
        }

        $editForm = $this->createForm(new SiteType(),$entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', "Le site a bien été modifié.");
            return $this->redirect($this->generateUrl('site_show', array('id' => $id)));
        }

        return array(
            'entity'        => $entity,
            'controller'    => 'sites',
            'edit_form'     => $editForm->createView(),
        );
    }

    /**
     * Function which call a service to archive
     * a site
     * @param $id
     * @Route("/disable/{id}", name="site_delete")
     * @return mixed
     */
    public function deleteAction($id)
    {
        $site = $this->get('backend.localisation')->getSite($id);

        if (!$site) {
            $this->get('session')->getFlashBag()->add('error', "Le site n'existe pas.");
            return $this->redirect($this->generateUrl('site'));
        }

        $this->get("backend.localisation")->disableLocalisation($site);
        $this->get('session')->getFlashBag()->add('info', "Le site a bien été archivé .");
        return $this->redirect($this->generateUrl('site'));
    }

    /**
     * Function which call a service to archive
     * a site
     * @param $id
     * @Route("/enable/{id}", name="site_enable")
     * @return mixed
     */
    public function activeAction($id)
    {
        $site = $this->get('backend.localisation')->getSite($id);

        if (!$site) {
            $this->get('session')->getFlashBag()->add('error', "Le site n'existe pas.");
            return $this->redirect($this->generateUrl('site'));
        }

        $this->get("backend.localisation")->enableLocalisation($site);
        $this->get('session')->getFlashBag()->add('info', "Le site a bien été activé .");
        return $this->redirect($this->generateUrl('site'));
    }
}
