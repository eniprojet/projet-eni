<?php

namespace Osmose\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Osmose\BackendBundle\Entity\Room;
use Osmose\BackendBundle\Form\RoomType;

/**
 * Room controller.
 *
 * @Route("/room")
 */
class RoomController extends Controller
{

    /**
     * Lists all Room entities.
     *
     * @Route("/", name="room")
     * @Method("GET")
     * @Template()return array(
    'entities'      => $entities,
    'controller'    => "rooms",
    'title_page'    => 'Liste des salles',
    );
     */
    public function indexAction()
    {
        $entities = $this->get('backend.room')->getAllRooms();

        if ($this->get('security.authorization_checker')->isGranted('ROLE_FORMATEUR')) {
            $entities = $this->get('backend.room')->getAllActiveRooms();
        }

        return array(
            'entities'      => $entities,
            'controller'    => "rooms",
            'title_page'    => 'Liste des salles',
        );
    }
    /**
     * Creates a new Room entity.
     *
     * @Route("/new", name="room_new")
     * @Template("BackendBundle:Room:new.html.twig")
     * @param Request $request
     * @Security("has_role('ROLE_ADMIN')")
     * @return mixed
     */
    public function createAction(Request $request)
    {
        $entity = new Room();
        $form = $this->createForm(new RoomType(), $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', "La salle a bien été créée.");
            return $this->redirect($this->generateUrl('room_show', array('id' => $entity->getId())));
        }

        return array(
            'entity'        => $entity,
            'controller'    => "rooms",
            'form'          => $form->createView(),
        );
    }

    /**
     * Finds and displays a Room entity.
     *
     * @Route("/{id}", name="room_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $entity = $this->get('backend.room')->getRoom($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "La salle n'existe pas.");
            return $this->redirect($this->generateUrl('room'));
        }

        return array(
            'entity'        => $entity,
            'controller'    => "rooms",
        );
    }

    /**
     * Edits an existing Room entity.
     *
     * @Route("/edit/{id}", name="room_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("BackendBundle:Room:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Room')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "La salle n'existe pas.");
            return $this->redirect($this->generateUrl('room'));
        }

        $editForm = $this->createForm(new RoomType(),$entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', "La salle a bien été mise à jour.");
            return $this->redirect($this->generateUrl('room_show', array('id' => $id)));
        }

        return array(
            'entity'        => $entity,
            'controller'    => "rooms",
            'title_page'    => "Modification de la salle",
            'edit_form'     => $editForm->createView(),
        );
    }

    /**
     * Deletes a Room entity.
     * @param $id
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/delete/{id}", name="room_delete")
     * @return mixed
     */
    public function deleteAction($id)
    {
        $this->get('backend.room')->disableRoom($id);
        $this->get('session')->getFlashBag()->add('error', "La salle a bien été archivée.");
        return $this->redirectToRoute('room');

    }

    /**
     * Deletes a Room entity.
     * @param $id
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/enable/{id}", name="room_enable")
     * @return mixed
     */
    public function enableAction($id)
    {
        $this->get('backend.room')->enableRoom($id);
        $this->get('session')->getFlashBag()->add('info', "La salle a bien été activée.");
        return $this->redirectToRoute('room');

    }

    /**
     * Display a list of rooms by site
     * @param $id
     * @Route("/by-site/{id}", name="rooms_by_site")
     * @Template("BackendBundle:Room:list-by-site.html.twig")
     * @return mixed
     */
    public function displayRoomsBySite($id)
    {
        $rooms = $this->get('backend.room')->getRoomsBySite($id);
        $site = $this->get('backend.localisation')->getSite($id);

        return array(
            'rooms'         => $rooms,
            'controller'    => "rooms",
            'title_page'    => $site->getName()
        );

    }
}
