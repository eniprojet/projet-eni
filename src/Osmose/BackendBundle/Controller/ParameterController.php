<?php

namespace Osmose\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Osmose\BackendBundle\Entity\Slot;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use \DateTime;
use Osmose\BackendBundle\Entity\Room;
use Osmose\BackendBundle\Form\RoomType;

/**
 * Room controller.
 *
 * @Route("/parameter")
 */
class ParameterController extends Controller
{

    /**
     * Lists all Room entities.
     *
     * @Route("/", name="parameter")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $entities =null;
        return array(
            'entities'      => $entities,
            'controller'    => "parameter",
            'title_page'    => 'Param',
        );
    }
    /**
     * Creates a new Room entity.
     *
     * @Route("/", name="param_do_import_csv")
     * @Method("POST")
     * @Template("BackendBundle:Parameter:doimportcsv.html.twig")
     * @param Request $request
     * @Security("has_role('ROLE_ADMIN')")
     * @return mixed
     */
    public function importCSV(Request $request)
    {
        $filePath = $request->files->get("userFile")->getRealPath();
        //$userManager = $this->get('fos_user.user_manager');
        //$users = $userManager->findUsers();
        //dump($users);
        $em = $this->getDoctrine()->getManager();

        $firstLineDone=false;

        if (($handle = fopen($filePath, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                dump($data);
                if($firstLineDone )
                {

                    $slot = new Slot();
                    $titre= utf8_encode($data[0]);
                    dump($titre);die();
                    $debut=utf8_encode($data[1]);
                    $fin=utf8_encode($data[3]);
                    $formateur=utf8_encode($data[9]);
                    $categorie=utf8_encode($data[14]);

                    $slot->setNote(utf8_encode($titre));

                    $name = explode(" ", utf8_encode($formateur));
                    $user = $em->getRepository("UserBundle:User")->findOneBy(array('lastname' => strtolower($name[0])));
                    $slot->setTrainer($user);




                    $dateTimeDebut = new DateTime();
                    $dateTimeDebut->setTimestamp(strtotime($debut));

                    $dateTimeFin = new DateTime();
                    $dateTimeFin->setTimestamp(strtotime($fin));

                    $slot->setStartsOn($dateTimeDebut);

                    $slot->setEndsOn($dateTimeFin);
                    dump($slot);
                   // $em->persist($slot);

                    // Étape 2 : On « flush » tout ce qui a été persisté avant
                }
                else
                {
                    $firstLineDone=true;
                }


               }
            $em->flush();
            fclose($handle);
            die();
        }

        return array(
            'controller'    => 'parameter',
            'title_page'    => "Import CSV"
        );
    }

    /**
     * Sync ENI
     *
     * @Route("/sync", name="synchronize_sql")
     * @Method("POST")
     * @Template("BackendBundle:Parameter:dosync.html.twig")
     * @param Request $request
     * @Security("has_role('ROLE_ADMIN')")
     * @return mixed
     */
    public function syncSQL(Request $request)
    {
        return array(
            'controller'    => 'parameter',
            'title_page'    => "Import et Synchronisation"
        );
    }

    /**
     * Display all user's connected slots
     *
     * @Route("/import_csv/", name="import_csv")
     * @Template("BackendBundle:Parameter:importcsv.html.twig")
     */
    public function MySlots()
    {

        return array(
            'controller'    => 'parameter',
            'title_page'    => "Import CSV"
        );
    }

}
