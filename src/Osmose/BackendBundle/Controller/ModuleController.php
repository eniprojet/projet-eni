<?php

namespace Osmose\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Osmose\BackendBundle\Entity\Module;
use Osmose\BackendBundle\Form\ModuleType;
use Symfony\Component\HttpFoundation\Response;
/**
 * Module controller.
 *
 * @Route("/module")
 */
class ModuleController extends Controller
{

    /**
     * Lists all Module entities.
     *
     * @Route("/", name="module")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Module')->findAll();

        return array(
            'entities' => $entities,
            'controller'  => 'modules',
            'title_page'  => "Liste des modules"
        );
    }
    /**
     * Creates a new Module entity.
     *
     * @Route("/new", name="module_new")
     * @Template("BackendBundle:Module:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Module();
        $form = $this->createForm(new ModuleType(),$entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', "Le module a bien été créée.");
            return $this->redirect($this->generateUrl('module_show', array('id' => $entity->getId())));
        }

        return array(
            'entity'        => $entity,
            'form'          => $form->createView(),
            'controller'    => 'modules',
            'title_page'    => "Créer un module"
        );
    }

    /**
     * Finds and displays a Module entity.
     *
     * @Route("/{id}", name="module_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Module')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "Le module n'existe pas.");
            return $this->redirect($this->generateUrl('module'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'controller'  => 'modules',
            'title_page'  => "Supprimer un module"
        );
    }

    /**
     * Displays a form to edit an existing Module entity.
     *
     * @Route("/{id}/edit", name="module_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Module')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "Le module n'existe pas.");
            return $this->redirect($this->generateUrl('module'));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'controller'  => 'modules',
            'title_page'  => "Modifier un module"
        );
    }

    /**
    * Creates a form to edit a Module entity.
    *
    * @param Module $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Module $entity)
    {
        $form = $this->createForm(new ModuleType(), $entity, array(
            'action' => $this->generateUrl('module_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing Module entity.
     *
     * @Route("/{id}", name="module_update")
     * @Method("PUT")
     * @Template("BackendBundle:Module:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Module')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "Le module n'existe pas.");
            return $this->redirect($this->generateUrl('module'));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', "Le module a bien été modifié.");
            return $this->redirect($this->generateUrl('module_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'controller'  => 'modules',
            'title_page'  => "Modifier un module"
        );
    }
    /**
     * Deletes a Module entity.
     *
     * @Route("/{id}", name="module_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Module')->find($id);

            if (!$entity) {
                $this->get('session')->getFlashBag()->add('error', "Le module n'existe pas.");
                return $this->redirect($this->generateUrl('module'));
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('module'));
    }

    /**
     * Creates a form to delete a Module entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('module_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
