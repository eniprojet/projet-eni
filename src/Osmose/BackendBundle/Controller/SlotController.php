<?php

namespace Osmose\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Osmose\BackendBundle\Entity\Slot;
use Osmose\BackendBundle\Form\SlotType;

/**
 * Slot controller.
 *
 * @Route("/slot")
 */
class SlotController extends Controller
{

    /**
     * Lists all Slot entities.
     *
     * @Route("/", name="slot")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $slots = $this->get('backend.slot')->getAllSlots();

        return array(
            'slots'         => $slots,
            'controller'    => 'slots',
            'title_page'    => "Créneaux enregistrés"
        );
    }
    /**
     * Creates a new Slot entity.
     *
     * @Route("/", name="slot_create")
     * @Method("POST")
     * @Template("BackendBundle:Slot:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Slot();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('slot_show', array('id' => $entity->getId())));
        }

        return array(
            'entity'        => $entity,
            'controller'    => 'slots',
            'form'          => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Slot entity.
     *
     * @param Slot $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Slot $entity)
    {
        $form = $this->createForm(new SlotType(), $entity, array(
            'action' => $this->generateUrl('slot_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Slot entity.
     *
     * @Route("/new", name="slot_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Slot();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity'        => $entity,
            'controller'    => 'slots',
            'form'          => $form->createView(),
        );
    }

    /**
     * Finds and displays a Slot entity.
     *
     * @Route("/{id}", name="slot_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Slot')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Slot entity.');
        }

        return array(
            'entity'        => $entity,
            'controller'    => 'slots',
        );
    }

    /**
     * Displays a form to edit an existing Slot entity.
     *
     * @Route("/{id}/edit", name="slot_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Slot')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Slot entity.');
        }

        $editForm = $this->createEditForm($entity);

        return array(
            'entity'        => $entity,
            'controller'    => 'slots',
            'edit_form'     => $editForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Slot entity.
    *
    * @param Slot $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Slot $entity)
    {
        $form = $this->createForm(new SlotType(), $entity, array(
            'action' => $this->generateUrl('slot_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Slot entity.
     *
     * @Route("/{id}", name="slot_update")
     * @Method("PUT")
     * @Template("BackendBundle:Slot:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Slot')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Slot entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('slot_edit', array('id' => $id)));
        }

        return array(
            'entity'        => $entity,
            'controller'    => 'slots',
            'edit_form'     => $editForm->createView(),
        );
    }

    /**
     * Display all user's connected slots
     *
     * @Route("/my-slots/", name="my_slots")
     * @Template("BackendBundle:Slot:my-slots.html.twig")
     */
    public function MySlots()
    {
        $slots = $this->get('backend.slot')->getMySlots($this->getUser());
        return array(
            'slots'         => $slots,
            'controller'    => 'slots',
            'title_page'    => "Mes créneaux de formation"
        );
    }
}
