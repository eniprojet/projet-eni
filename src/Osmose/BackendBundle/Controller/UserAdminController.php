<?php

namespace Osmose\BackendBundle\Controller;

use Osmose\BackendBundle\Form\UserEditType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Osmose\UserBundle\Entity\User;
use Osmose\BackendBundle\Form\UserType;

/**
 * User controller.
 *
 * @Route("/user")
 */
class UserAdminController extends Controller
{

    /**
     * Lists all users.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UserBundle:User')->findAll();

        return array(
            'entities' => $entities,
            'controller'    => "utilisateurs",
            'title_page'    => "Liste des utilisateurs"
        );
    }
    /**
     * Creates a new User entity.
     *
     * @Route("/", name="user_create")
     * @Method("POST")
     * @Template("BackendBundle:UserAdmin:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', "L'utilisateur a bien été créé.");
            return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
        }

        return array(
            'entity'        => $entity,
            'controller'    => "utilisateurs",
            'form'          => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Room entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        return $form;
    }
    /**
     * Display a user's slots
     *
     * @Route("/{id}/his-slots/", name="his_slots")
     * @Template("BackendBundle:Slot:my-slots.html.twig")
     */
    public function HisSlots($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);
        $slots = $this->get('backend.slot')->getMySlots($user);
        return array(
            'slots'         => $slots,
            'controller'    => 'slots',
            'title_page'    => "Les sessions de ".$user->getFirstname()." ".$user->getLastname()
        );
    }
    /**
     * Displays a form to create a new Room entity.
     *
     * @Route("/new", name="user_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new User();

        $form = $this->createCreateForm($entity);

        return array(
            'entity'        => $entity,
            'form'          => $form->createView(),
            'controller'    => "utilisateurs",
            'title_page'    => "Ajout d'un utilisateur"
        );
    }

    /**
     * Finds and displays a Room entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     * @Template("BackendBundle:UserAdmin:show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UserBundle:User')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "L'utilisateur n'existe pas.");
            return $this->redirect($this->generateUrl('user_index'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'        => $entity,
            'controller'    => "utilisateurs",
            'delete_form'   => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Room entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UserBundle:User')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "L'utilisateur n'existe pas.");
            return $this->redirect($this->generateUrl('user_index'));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'        => $entity,
            'edit_form'     => $editForm->createView(),
            'controller'    => "utilisateurs",
            'delete_form'   => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Room entity.
    *
    * @param Room $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserEditType(), $entity, array(
            'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing Room entity.
     *
     * @Route("/{id}/update", name="user_update")
     * @Method("PUT")
     * @Template("BackendBundle:User:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UserBundle:User')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "L'utilisateur n'existe pas.");
            return $this->redirect($this->generateUrl('user_index'));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', "L'utilisateur a bien été modifié.");
            return $this->redirect($this->generateUrl('user_show', array('id' => $id)));
        }

        return array(
            'entity'        => $entity,
            'controller'    => "utilisateurs",
            'edit_form'     => $editForm->createView(),
            'delete_form'   => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a user entity.
     *
     * @Route("/{id}/disable", name="user_disable")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {


            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UserBundle:User')->find($id);

            if (!$entity) {
                $this->get('session')->getFlashBag()->add('error', "L'utilisateur n'existe pas.");
                return $this->redirect($this->generateUrl('user_index'));
            }


        $this->get("backend.user")->disableUser($entity);
        $this->get('session')->getFlashBag()->add('info', "L'utilisateur a bien été archivé .");



        return $this->redirect($this->generateUrl('user_index'));
    }
    /**
     * Deletes a user entity.
     *
     * @Route("/{id}/enable", name="user_enable")
     * @Method("GET")
     */
    public function enableAction(Request $request, $id)
    {


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UserBundle:User')->find($id);

        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error', "L'utilisateur n'existe pas.");
            return $this->redirect($this->generateUrl('user_index'));
        }


        $this->get("backend.user")->enableUser($entity);
        $this->get('session')->getFlashBag()->add('info', "L'utilisateur a bien été activé .");



        return $this->redirect($this->generateUrl('user_index'));
    }

    /**
     * Creates a form to delete a Room entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_disable', array('id' => $id)))
            ->setMethod('GET')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
