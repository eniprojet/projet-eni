<?php

namespace Osmose\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formation
 *
 * @ORM\Table(name="eni_formation")
 * @ORM\Entity(repositoryClass="Osmose\BackendBundle\Entity\FormationRepository")
 */
class Formation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="CodeFormation", type="string", length=8)
     */
    private $codeFormation;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleLong", type="string", length=200)
     */
    private $libelleLong;

    /**
     * @var string
     *
     * @ORM\Column(name="LibelleCourt", type="string", length=50)
     */
    private $libelleCourt;

    /**
     * @var integer
     *
     * @ORM\Column(name="DureeEnHeures", type="smallint")
     */
    private $dureeEnHeures;

    /**
     * @var float
     *
     * @ORM\Column(name="TauxHoraire", type="float")
     */
    private $tauxHoraire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var string
     *
     * @ORM\Column(name="CodeTitre", type="string", length=8)
     */
    private $codeTitre;

    /**
     * @var float
     *
     * @ORM\Column(name="PrixPublicEnCours", type="float")
     */
    private $prixPublicEnCours;

    /**
     * @var integer
     *
     * @ORM\Column(name="HeuresCentre", type="smallint")
     */
    private $heuresCentre;

    /**
     * @var integer
     *
     * @ORM\Column(name="HeuresStage", type="smallint")
     */
    private $heuresStage;

    /**
     * @var integer
     *
     * @ORM\Column(name="SemainesCentre", type="integer")
     */
    private $semainesCentre;

    /**
     * @var integer
     *
     * @ORM\Column(name="SemainesStage", type="integer")
     */
    private $semainesStage;

    /**
     * @var integer
     *
     * @ORM\Column(name="DureeEnSemaines", type="integer")
     */
    private $dureeEnSemaines;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateModif", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateModif;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Archiver", type="boolean")
     */
    private $archiver = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ECFaPasser", type="boolean", nullable=true)
     */
    private $eCFaPasser;


    public function __construct()
    {
        $this->dateCreation = new \DateTime("now");
        $this->dateModif = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeFormation
     *
     * @param string $codeFormation
     *
     * @return Formation
     */
    public function setCodeFormation($codeFormation)
    {
        $this->codeFormation = $codeFormation;

        return $this;
    }

    /**
     * Get codeFormation
     *
     * @return string
     */
    public function getCodeFormation()
    {
        return $this->codeFormation;
    }

    /**
     * Set libelleLong
     *
     * @param string $libelleLong
     *
     * @return Formation
     */
    public function setLibelleLong($libelleLong)
    {
        $this->libelleLong = $libelleLong;

        return $this;
    }

    /**
     * Get libelleLong
     *
     * @return string
     */
    public function getLibelleLong()
    {
        return $this->libelleLong;
    }

    /**
     * Set libelleCourt
     *
     * @param string $libelleCourt
     *
     * @return Formation
     */
    public function setLibelleCourt($libelleCourt)
    {
        $this->libelleCourt = $libelleCourt;

        return $this;
    }

    /**
     * Get libelleCourt
     *
     * @return string
     */
    public function getLibelleCourt()
    {
        return $this->libelleCourt;
    }

    /**
     * Set dureeEnHeures
     *
     * @param integer $dureeEnHeures
     *
     * @return Formation
     */
    public function setDureeEnHeures($dureeEnHeures)
    {
        $this->dureeEnHeures = $dureeEnHeures;

        return $this;
    }

    /**
     * Get dureeEnHeures
     *
     * @return integer
     */
    public function getDureeEnHeures()
    {
        return $this->dureeEnHeures;
    }

    /**
     * Set tauxHoraire
     *
     * @param float $tauxHoraire
     *
     * @return Formation
     */
    public function setTauxHoraire($tauxHoraire)
    {
        $this->tauxHoraire = $tauxHoraire;

        return $this;
    }

    /**
     * Get tauxHoraire
     *
     * @return float
     */
    public function getTauxHoraire()
    {
        return $this->tauxHoraire;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Formation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set codeTitre
     *
     * @param string $codeTitre
     *
     * @return Formation
     */
    public function setCodeTitre($codeTitre)
    {
        $this->codeTitre = $codeTitre;

        return $this;
    }

    /**
     * Get codeTitre
     *
     * @return string
     */
    public function getCodeTitre()
    {
        return $this->codeTitre;
    }

    /**
     * Set prixPublicEnCours
     *
     * @param float $prixPublicEnCours
     *
     * @return Formation
     */
    public function setPrixPublicEnCours($prixPublicEnCours)
    {
        $this->prixPublicEnCours = $prixPublicEnCours;

        return $this;
    }

    /**
     * Get prixPublicEnCours
     *
     * @return float
     */
    public function getPrixPublicEnCours()
    {
        return $this->prixPublicEnCours;
    }

    /**
     * Set heuresCentre
     *
     * @param integer $heuresCentre
     *
     * @return Formation
     */
    public function setHeuresCentre($heuresCentre)
    {
        $this->heuresCentre = $heuresCentre;

        return $this;
    }

    /**
     * Get heuresCentre
     *
     * @return integer
     */
    public function getHeuresCentre()
    {
        return $this->heuresCentre;
    }

    /**
     * Set heuresStage
     *
     * @param integer $heuresStage
     *
     * @return Formation
     */
    public function setHeuresStage($heuresStage)
    {
        $this->heuresStage = $heuresStage;

        return $this;
    }

    /**
     * Get heuresStage
     *
     * @return integer
     */
    public function getHeuresStage()
    {
        return $this->heuresStage;
    }

    /**
     * Set semainesCentre
     *
     * @param integer $semainesCentre
     *
     * @return Formation
     */
    public function setSemainesCentre($semainesCentre)
    {
        $this->semainesCentre = $semainesCentre;

        return $this;
    }

    /**
     * Get semainesCentre
     *
     * @return integer
     */
    public function getSemainesCentre()
    {
        return $this->semainesCentre;
    }

    /**
     * Set semainesStage
     *
     * @param integer $semainesStage
     *
     * @return Formation
     */
    public function setSemainesStage($semainesStage)
    {
        $this->semainesStage = $semainesStage;

        return $this;
    }

    /**
     * Get semainesStage
     *
     * @return integer
     */
    public function getSemainesStage()
    {
        return $this->semainesStage;
    }

    /**
     * Set dureeEnSemaines
     *
     * @param integer $dureeEnSemaine
     *
     * @return Formation
     */
    public function setDureeEnSemaines($dureeEnSemaines)
    {
        $this->dureeEnSemaines = $dureeEnSemaines;

        return $this;
    }

    /**
     * Get dureeEnSemaines
     *
     * @return integer
     */
    public function getDureeEnSemaines()
    {
        return $this->dureeEnSemaines;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return Formation
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * Set archiver
     *
     * @param boolean $archiver
     *
     * @return Formation
     */
    public function setArchiver($archiver)
    {
        $this->archiver = $archiver;

        return $this;
    }

    /**
     * Get archiver
     *
     * @return boolean
     */
    public function getArchiver()
    {
        return $this->archiver;
    }

    /**
     * Set eCFaPasser
     *
     * @param boolean $eCFaPasser
     *
     * @return Formation
     */
    public function setECFaPasser($eCFaPasser)
    {
        $this->eCFaPasser = $eCFaPasser;

        return $this;
    }

    /**
     * Get eCFaPasser
     *
     * @return boolean
     */
    public function getECFaPasser()
    {
        return $this->eCFaPasser;
    }

    public function __toString() {
        return $this->codeFormation;
    }
}
