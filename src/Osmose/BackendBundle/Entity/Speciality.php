<?php

namespace Osmose\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Speciality
 *
 * @ORM\Table(name="osmose_speciality_type")
 * @ORM\Entity(repositoryClass="Osmose\BackendBundle\Entity\SpecialityRepository")
 */
class Speciality
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label_speciality", type="string", length=50)
     */
    private $labelSpeciality;

    /**
     * @var string
     *
     * @ORM\Column(name="code_type", type="string", length=10)
     */
    private $type;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set labelSpeciality
     *
     * @param string $labelSpeciality
     *
     * @return Speciality
     */
    public function setLabelSpeciality($labelSpeciality)
    {
        $this->labelSpeciality = $labelSpeciality;

        return $this;
    }

    /**
     * Get labelSpeciality
     *
     * @return string
     */
    public function getLabelSpeciality()
    {
        return $this->labelSpeciality;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Speciality
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
