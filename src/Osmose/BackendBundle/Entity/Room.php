<?php

namespace Osmose\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Osmose\CoreBundle\Entity\AbstractOsmoseEntity;

/**
 * Room
 *
 * @ORM\Table(name="osmose_room")
 * @ORM\Entity(repositoryClass="Osmose\BackendBundle\Entity\RoomRepository")
 */
class Room extends AbstractOsmoseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code_salle", type="string", length=50)
     */
    private $codeSalle;

    /**
     * @var integer
     *
     * @ORM\Column(name="seats", type="integer")
     */
    private $seats;

    /**
     * @var string
     *
     * @ORM\Column(name="computers", type="text",nullable=true)
     */
    private $computers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="projector", type="boolean",nullable=true)
     */
    private $projector;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=150)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="computers_models", type="string", length=45, nullable=true)
     */
    private $computersModels;

    /**
     * @var string
     *
     * @ORM\Column(name="computers_ram", type="string", length=45,nullable=true)
     */
    private $computersRam;

    /**
     * @var string
     *
     * @ORM\Column(name="computers_disks", type="string", length=90,nullable=true)
     */
    private $computersDisks;

    /**
     * @var string
     *
     * @ORM\Column(name="computers_cpu", type="string", length=45,nullable=true)
     */
    private $computersCpu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="warranty_end", type="datetime",nullable=true)
     */
    private $warrantyEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="site_id", type="integer",nullable=true)
     */
    private $siteId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="archived", type="boolean")
     */
    private $archived = false;

    /**
     * @var site
     *
     * @ORM\ManyToOne(targetEntity="Site")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id",nullable=true)
     */
    private $site;


    /**
     *
     * @ORM\ManyToOne(targetEntity="Osmose\BackendBundle\Entity\Speciality")
     * @ORM\JoinColumn(name="speciality_id", referencedColumnName="id")
     */
    private $speciality;


    public function __construct()
    {
        $this->archived = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set seats
     *
     * @param integer $seats
     *
     * @return Room
     */
    public function setSeats($seats)
    {
        $this->seats = $seats;

        return $this;
    }

    /**
     * Get seats
     *
     * @return integer
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * Set computers
     *
     * @param string $computers
     *
     * @return Room
     */
    public function setComputers($computers)
    {
        $this->computers = $computers;

        return $this;
    }

    /**
     * Get computers
     *
     * @return string
     */
    public function getComputers()
    {
        return $this->computers;
    }

    /**
     * Set projector
     *
     * @param boolean $projector
     *
     * @return Room
     */
    public function setProjector($projector)
    {
        $this->projector = $projector;

        return $this;
    }

    /**
     * Get projector
     *
     * @return boolean
     */
    public function getProjector()
    {
        return $this->projector;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Room
     */
    public function setNumber($number)
    {
        $this->number = $number;
        $this->codeSalle = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set computersModels
     *
     * @param string $computersModels
     *
     * @return Room
     */
    public function setComputersModels($computersModels)
    {
        $this->computersModels = $computersModels;

        return $this;
    }

    /**
     * Get computersModels
     *
     * @return string
     */
    public function getComputersModels()
    {
        return $this->computersModels;
    }

    /**
     * Set computersRam
     *
     * @param string $computersRam
     *
     * @return Room
     */
    public function setComputersRam($computersRam)
    {
        $this->computersRam = $computersRam;

        return $this;
    }

    /**
     * Get computersRam
     *
     * @return string
     */
    public function getComputersRam()
    {
        return $this->computersRam;
    }

    /**
     * Set computersDisks
     *
     * @param string $computersDisks
     *
     * @return Room
     */
    public function setComputersDisks($computersDisks)
    {
        $this->computersDisks = $computersDisks;

        return $this;
    }

    /**
     * Get computersDisks
     *
     * @return string
     */
    public function getComputersDisks()
    {
        return $this->computersDisks;
    }

    /**
     * Set computersCpu
     *
     * @param string $computersCpu
     *
     * @return Room
     */
    public function setComputersCpu($computersCpu)
    {
        $this->computersCpu = $computersCpu;

        return $this;
    }

    /**
     * Get computersCpu
     *
     * @return string
     */
    public function getComputersCpu()
    {
        return $this->computersCpu;
    }

    /**
     * Set network
     *
     * @param boolean $network
     *
     * @return Room
     */
    public function setNetwork($network)
    {
        $this->network = $network;

        return $this;
    }

    /**
     * Get network
     *
     * @return boolean
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * Set development
     *
     * @param boolean $development
     *
     * @return Room
     */
    public function setDevelopment($development)
    {
        $this->development = $development;

        return $this;
    }

    /**
     * Get development
     *
     * @return boolean
     */
    public function getDevelopment()
    {
        return $this->development;
    }

    /**
     * Set warrantyEnd
     *
     * @param \DateTime $warrantyEnd
     *
     * @return Room
     */
    public function setWarrantyEnd($warrantyEnd)
    {
        $this->warrantyEnd = $warrantyEnd;

        return $this;
    }

    /**
     * Get warrantyEnd
     *
     * @return \DateTime
     */
    public function getWarrantyEnd()
    {
        return $this->warrantyEnd;
    }

    /**
     * Set siteId
     *
     * @param integer $siteId
     *
     * @return Room
     */
    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;

        return $this;
    }

    /**
     * Get siteId
     *
     * @return integer
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param \Osmose\BackendBundle\Entity\Site $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * @return \Osmose\BackendBundle\Entity\Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param boolean $archived
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;
    }

    /**
     * @return boolean $archived
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * Set codeSalle
     *
     * @param string $codeSalle
     *
     * @return Room
     */
    public function setCodeSalle($codeSalle)
    {
        $this->codeSalle = $codeSalle;

        return $this;
    }

    /**
     * Get codeSalle
     *
     * @return string
     */
    public function getCodeSalle()
    {
        return $this->codeSalle;
    }

    /**
     * Set speciality
     *
     * @param \Osmose\BackendBundle\Entity\Speciality $speciality
     *
     * @return Room
     */
    public function setSpeciality(\Osmose\BackendBundle\Entity\Speciality $speciality = null)
    {
        $this->speciality = $speciality;

        return $this;
    }

    /**
     * Get speciality
     *
     * @return \Osmose\BackendBundle\Entity\Speciality
     */
    public function getSpeciality()
    {
        return $this->speciality;
    }
}
