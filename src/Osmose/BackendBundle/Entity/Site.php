<?php

namespace Osmose\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Osmose\CoreBundle\Entity\AbstractOsmoseEntity;

/**
 * Site
 *
 * @ORM\Table(name="osmose_site")
 * @ORM\Entity(repositoryClass="Osmose\BackendBundle\Entity\SiteRepository")
 */
class Site extends AbstractOsmoseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="opening_hour", type="datetime")
     */
    private $openingHour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closing_hour", type="datetime")
     */
    private $closingHour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="break_start", type="datetime")
     */
    private $breakStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="break_end", type="datetime")
     */
    private $breakEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=150)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", options={"default":true})
     */
    private $enabled;

    /**
     * Site constructor.
     */
    public function __construct()
    {
        $this->enabled = true;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Site
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set openingHour
     *
     * @param \DateTime $openingHour
     *
     * @return Site
     */
    public function setOpeningHour($openingHour)
    {
        $this->openingHour = $openingHour;

        return $this;
    }

    /**
     * Get openingHour
     *
     * @return \DateTime
     */
    public function getOpeningHour()
    {
        return $this->openingHour;
    }

    /**
     * Set closingHour
     *
     * @param \DateTime $closingHour
     *
     * @return Site
     */
    public function setClosingHour($closingHour)
    {
        $this->closingHour = $closingHour;

        return $this;
    }

    /**
     * Get closingHour
     *
     * @return \DateTime
     */
    public function getClosingHour()
    {
        return $this->closingHour;
    }

    /**
     * Set breakStart
     *
     * @param \DateTime $breakStart
     *
     * @return Site
     */
    public function setBreakStart($breakStart)
    {
        $this->breakStart = $breakStart;

        return $this;
    }

    /**
     * Get breakStart
     *
     * @return \DateTime
     */
    public function getBreakStart()
    {
        return $this->breakStart;
    }

    /**
     * Set breakEnd
     *
     * @param \DateTime $breakEnd
     *
     * @return Site
     */
    public function setBreakEnd($breakEnd)
    {
        $this->breakEnd = $breakEnd;

        return $this;
    }

    /**
     * Get breakEnd
     *
     * @return \DateTime
     */
    public function getBreakEnd()
    {
        return $this->breakEnd;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Site
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Site
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    public function __toString() {
        return (string) $this->getCity();
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }


}

