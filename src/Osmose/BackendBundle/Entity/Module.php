<?php

namespace Osmose\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Module
 *
 * @ORM\Table(name="eni_module")
 * @ORM\Entity(repositoryClass="Osmose\BackendBundle\Entity\ModuleRepository")
 */
class Module
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="libelle", type="string", length=200)
     */
    private $libelle;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @ORM\Column(name="duree_en_heures", type="integer")
     */
    private $dureeEnHeures;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_creation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @ORM\Column(name="duree_en_semaines", type="smallint")
     */
    private $dureeEnSemaines;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_public_en_cours", type="float")
     */
    private $prixPublicEnCours;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="libelle_court", type="string", length=20)
     */
    private $libelleCourt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modif", type="datetime",nullable=true)
     */
    private $dateModif;

    /**
     * @var boolean
     *
     * @ORM\Column(name="archiver", type="boolean")
     */
    private $archiver;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="detail", type="text")
     */
    private $detail;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Osmose\BackendBundle\Entity\Speciality")
     * @ORM\JoinColumn(name="speciality_id", referencedColumnName="id")
     */
    private $speciality;

    

    public function __construct()
    {
        $this->archiver = false;
        $this->dateCreation = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Module
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set dureeEnHeures
     *
     * @param integer $dureeEnHeures
     *
     * @return Module
     */
    public function setDureeEnHeures($dureeEnHeures)
    {
        $this->dureeEnHeures = $dureeEnHeures;

        return $this;
    }

    /**
     * Get dureeEnHeures
     *
     * @return integer
     */
    public function getDureeEnHeures()
    {
        return $this->dureeEnHeures;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Module
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dureeEnSemaines
     *
     * @param integer $dureeEnSemaines
     *
     * @return Module
     */
    public function setDureeEnSemaines($dureeEnSemaines)
    {
        $this->dureeEnSemaines = $dureeEnSemaines;

        return $this;
    }

    /**
     * Get dureeEnSemaines
     *
     * @return integer
     */
    public function getDureeEnSemaines()
    {
        return $this->dureeEnSemaines;
    }

    /**
     * Set prixPublicEnCours
     *
     * @param float $prixPublicEnCours
     *
     * @return Module
     */
    public function setPrixPublicEnCours($prixPublicEnCours)
    {
        $this->prixPublicEnCours = $prixPublicEnCours;

        return $this;
    }

    /**
     * Get prixPublicEnCours
     *
     * @return float
     */
    public function getPrixPublicEnCours()
    {
        return $this->prixPublicEnCours;
    }

    /**
     * Set libelleCourt
     *
     * @param string $libelleCourt
     *
     * @return Module
     */
    public function setLibelleCourt($libelleCourt)
    {
        $this->libelleCourt = $libelleCourt;

        return $this;
    }

    /**
     * Get libelleCourt
     *
     * @return string
     */
    public function getLibelleCourt()
    {
        return $this->libelleCourt;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return Module
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * Set archiver
     *
     * @param boolean $archiver
     *
     * @return Module
     */
    public function setArchiver($archiver)
    {
        $this->archiver = $archiver;

        return $this;
    }

    /**
     * Get archiver
     *
     * @return boolean
     */
    public function getArchiver()
    {
        return $this->archiver;
    }

    /**
     * Set detail
     *
     * @param string $detail
     *
     * @return Module
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set speciality
     *
     * @param \Osmose\BackendBundle\Entity\Speciality $speciality
     *
     * @return Module
     */
    public function setSpeciality(\Osmose\BackendBundle\Entity\Speciality $speciality = null)
    {
        $this->speciality = $speciality;

        return $this;
    }

    /**
     * Get speciality
     *
     * @return \Osmose\BackendBundle\Entity\Speciality
     */
    public function getSpeciality()
    {
        return $this->speciality;
    }
}
