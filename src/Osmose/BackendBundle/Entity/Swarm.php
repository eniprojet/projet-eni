<?php

namespace Osmose\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Osmose\CoreBundle\Entity\AbstractOsmoseEntity;

/**
 * Swarm
 *
 * @ORM\Table(name="osmose_swarm")
 * @ORM\Entity(repositoryClass="Osmose\BackendBundle\Entity\SwarmRepository")
 */
class Swarm extends AbstractOsmoseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code_promotion", type="string", length=8)
     */
    private $codePromotion;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=200)
     */
    private $libelle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="debut", type="datetime")
     */
    private $debut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin", type="datetime")
     */
    private $fin;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_public_affecte", type="float")
     */
    private $prixPublicAffecte;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modif", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateModif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreation;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_pec_affecte", type="float")
     */
    private $prixPecAffecte;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_finance_affecte", type="float")
     */
    private $prixFinanceAffecte;

    /**
     * @var site
     *
     * @ORM\ManyToOne(targetEntity="Formation")
     * @ORM\JoinColumn(name="formation_id", referencedColumnName="id", nullable=true)
     */
    private $formation;

    /**
     * @ORM\ManyToOne(targetEntity="Osmose\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="supervisor_id", referencedColumnName="id", nullable=true)
     */
    private $supervisor;


    /**
     * @var site
     *
     * @ORM\ManyToOne(targetEntity="Site")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id",nullable=true)
     */
    private $site;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set supervisor
     *
     * @param integer $supervisor
     *
     * @return Swarm
     */
    public function setSupervisor($supervisor)
    {
        $this->supervisor = $supervisor;

        return $this;
    }

    /**
     * Get supervisor
     *
     * @return integer
     */
    public function getSupervisor()
    {
        return $this->supervisor;
    }

    /**
     * Set site
     *
     * @param integer $site
     *
     * @return Slot
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set codePromotion
     *
     * @param string $codePromotion
     *
     * @return Swarm
     */
    public function setCodePromotion($codePromotion)
    {
        $this->codePromotion = $codePromotion;

        return $this;
    }

    /**
     * Get codePromotion
     *
     * @return string
     */
    public function getCodePromotion()
    {
        return $this->codePromotion;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Swarm
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set debut
     *
     * @param \DateTime $debut
     *
     * @return Swarm
     */
    public function setDebut($debut)
    {
        $this->debut = $debut;

        return $this;
    }

    /**
     * Get debut
     *
     * @return \DateTime
     */
    public function getDebut()
    {
        return $this->debut;
    }

    /**
     * Set fin
     *
     * @param \DateTime $fin
     *
     * @return Swarm
     */
    public function setFin($fin)
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * Get fin
     *
     * @return \DateTime
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Set prixPublicAffecte
     *
     * @param float $prixPublicAffecte
     *
     * @return Swarm
     */
    public function setPrixPublicAffecte($prixPublicAffecte)
    {
        $this->prixPublicAffecte = $prixPublicAffecte;

        return $this;
    }

    /**
     * Get prixPublicAffecte
     *
     * @return float
     */
    public function getPrixPublicAffecte()
    {
        return $this->prixPublicAffecte;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return Swarm
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Swarm
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set prixPecAffecte
     *
     * @param float $prixPecAffecte
     *
     * @return Swarm
     */
    public function setPrixPecAffecte($prixPecAffecte)
    {
        $this->prixPecAffecte = $prixPecAffecte;

        return $this;
    }

    /**
     * Get prixPecAffecte
     *
     * @return float
     */
    public function getPrixPecAffecte()
    {
        return $this->prixPecAffecte;
    }

    /**
     * Set prixFinanceAffecte
     *
     * @param float $prixFinanceAffecte
     *
     * @return Swarm
     */
    public function setPrixFinanceAffecte($prixFinanceAffecte)
    {
        $this->prixFinanceAffecte = $prixFinanceAffecte;

        return $this;
    }

    /**
     * Get prixFinanceAffecte
     *
     * @return float
     */
    public function getPrixFinanceAffecte()
    {
        return $this->prixFinanceAffecte;
    }

    /**
     * Set formation
     *
     * @param \Osmose\BackendBundle\Entity\Formation $formation
     *
     * @return Swarm
     */
    public function setFormation(\Osmose\BackendBundle\Entity\Formation $formation = null)
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * Get formation
     *
     * @return \Osmose\BackendBundle\Entity\Formation
     */
    public function getFormation()
    {
        return $this->formation;
    }
}
