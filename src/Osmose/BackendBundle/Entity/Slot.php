<?php

namespace Osmose\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Osmose\CoreBundle\Entity\AbstractOsmoseEntity;

/**
 * Slot
 *
 * @ORM\Table(name="osmose_slot")
 * @ORM\Entity(repositoryClass="Osmose\BackendBundle\Entity\SlotRepository")
 */
class Slot extends AbstractOsmoseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="starts_on", type="datetime")
     */
    private $startsOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ends_on", type="datetime")
     */
    private $endsOn;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text")
     */
    private $note;

    /**
     * @var room
     *
     * @ORM\ManyToOne(targetEntity="Room")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     */
    private $room;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Osmose\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="trainer_id", referencedColumnName="id")
     */
    private $trainer;

    /**
     * @var swarm
     *
     * @ORM\ManyToOne(targetEntity="Osmose\BackendBundle\Entity\Swarm")
     * @ORM\JoinColumn(name="swarm_id", referencedColumnName="id")
     */
    private $swarm;


    /**
     * @var module
     *
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id", nullable=true)
     */
    private $module;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startsOn
     *
     * @param \DateTime $startsOn
     *
     * @return Slot
     */
    public function setStartsOn($startsOn)
    {
        $this->startsOn = $startsOn;

        return $this;
    }

    /**
     * Get startsOn
     *
     * @return \DateTime
     */
    public function getStartsOn()
    {
        return $this->startsOn;
    }

    /**
     * Set endsOn
     *
     * @param \DateTime $endsOn
     *
     * @return Slot
     */
    public function setEndsOn($endsOn)
    {
        $this->endsOn = $endsOn;

        return $this;
    }

    /**
     * Get endsOn
     *
     * @return \DateTime
     */
    public function getEndsOn()
    {
        return $this->endsOn;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Slot
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set roomId
     *
     * @param integer $room
     *
     * @return Slot
     */
    public function setRoom($room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set trainer
     *
     * @param integer $trainer
     *
     * @return Slot
     */
    public function setTrainer($trainer)
    {
        $this->trainer = $trainer;

        return $this;
    }

    /**
     * Get trainer
     *
     * @return integer
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Set swarm
     *
     * @param integer $swarm
     *
     * @return Slot
     */
    public function setSwarm($swarm)
    {
        $this->swarm = $swarm;

        return $this;
    }

    /**
     * Get swarm
     *
     * @return swarm
     */
    public function getSwarm()
    {
        return $this->swarm;
    }

    /**
     * Set module
     *
     * @param \Osmose\BackendBundle\Entity\Module $module
     *
     * @return Slot
     */
    public function setModule(\Osmose\BackendBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return \Osmose\BackendBundle\Entity\Module
     */
    public function getModule()
    {
        return $this->module;
    }
}
