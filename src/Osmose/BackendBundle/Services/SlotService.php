<?php
/**
 * Created by PhpStorm.
 * User: Audrophe
 * Date: 03/08/2016
 * Time: 12:53
 */

namespace Osmose\BackendBundle\Services;

use Doctrine\ORM\EntityManager;
use Osmose\UserBundle\Entity\User;
use Osmose\BackendBundle\Entity\Slot;

class SlotService
{

    /**
     * @var EntityManager
     */
    private $doctrine;

    public $logger;

    /**
     * Localisation service constructor.
     *
     * @param EntityManager $doctrine
     */
    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Allow the back to get all the paths.
     *
     * @return array Osmose\BackendBundle\Entity\Slot[]
     */
    public function countSlots()
    {
        return $this->doctrine->getRepository('BackendBundle:Slot')->countSlots();
    }

    /**
     * getAllSlots
     *
     * @return array Osmose\BackendBundle\Entity\Slot[]
     */
    public function getAllSlots()
    {
        $this->logger->info(get_class($this).': Chargement des sessions');
        return $this->doctrine->getRepository('BackendBundle:Slot')->findAll();
    }

    /**
     * GetMySlots
     * @param User $user
     * @return array|\Osmose\BackendBundle\Entity\Slot[]
     */
    public function getMySlots(User $user)
    {
        $this->logger->info(get_class($this).': Chargement des sessions de l\'utilisateur id='.$user->getId());
        return $this->doctrine->getRepository('BackendBundle:Slot')->findBy(array(
            'trainer' => $user
        ));
    }

    /**
     * Get one slot
     * @param $id
     * @return Slot
     */
    public function getSlot($id)
    {
        $this->logger->info(get_class($this).': Chargement de la session id='.$id);
        $slot = $this->doctrine->getRepository("BackendBundle:Slot")->find($id);
        return $slot;
    }

    /**
     * Delete Slot
     * @param Slot $slot
     * @return integer
     */
    public function deleteSlot(Slot $slot)
    {
        $this->logger->info(get_class($this).': Suppression de la session id='.$slot->getId());
        $retour = 0;
        try {
            $this->doctrine->remove($slot);
            $this->doctrine->flush();
            $retour = 1;
        } catch(\Doctrine\DBAL\DBALException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }catch(\Doctrine\ORM\ORMException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }
        return $retour;
    }

    /**
     * Check if the room is available at startsOn EndOn
     * @param $roomId
     * @param $startsOn
     * @param $endsOn
     * @return boolean
     */
    public function isRoomAvailableAtStartsOnEndsOn($room, $start, $end, $updateSlotId){
        $boolResult = true;
        $slots = $this->doctrine->getRepository('BackendBundle:Slot')->findBy(array(
            'room' => $room
        ));
        foreach ($slots as $slot){
            if (($start < $slot->getStartsOn() && $end > $slot->getStartsOn()) || ($start > $slot->getStartsOn() && $start < $slot->getEndsOn())
             || ($start < $slot->getStartsOn() && $end > $slot->getEndsOn()) || ($start == $slot->getStartsOn())){
              if ($slot->getId() != $updateSlotId){
                $boolResult = false;
                break;
              }
            }
        }
        return $boolResult;
    }

}
