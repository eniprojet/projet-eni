<?php
/**
 * Created by PhpStorm.
 * User: Audrophe
 * Date: 03/08/2016
 * Time: 12:52
 */

namespace Osmose\BackendBundle\Services;

use Doctrine\ORM\EntityManager;

class RoomService
{

    /**
     * @var EntityManager
     */
    private $doctrine;
    public $logger;

    /**
     * Localisation service constructor.
     *
     * @param EntityManager $doctrine
     */
    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Allow the back to get all the paths.
     *
     * @return array Osmose\BackendBundle\Entity\Room[]
     */
    public function countRooms($id)
    {
        return $this->doctrine->getRepository('BackendBundle:Room')->countRooms($id);
    }

    /**
     * Allow the back to get all the paths.
     *
     * @return array Osmose\BackendBundle\Entity\Room[]
     */
    public function countActiveRooms()
    {
        return $this->doctrine->getRepository('BackendBundle:Room')->countActiveRooms();
    }

    /**
     * Function to display all the rooms
     * @return array|\Osmose\BackendBundle\Entity\Room[]
     */
    public function getAllRooms()
    {
        $this->logger->info(get_class($this).': Chargement des salles');
        return $this->doctrine->getRepository('BackendBundle:Room')->findAll();
    }

    /**
     * Function to display all the active rooms
     * @return array|\Osmose\BackendBundle\Entity\Room[]
     */
    public function getAllActiveRooms()
    {
        $this->logger->info(get_class($this).': Chargement des salles');
        return $this->doctrine->getRepository('BackendBundle:Room')->getActiveRooms();
    }

    /**
     * Function to display a room
     * @param $id
     * @return array|\Osmose\BackendBundle\Entity\Room[]
     */
    public function getRoom($id)
    {
        $this->logger->info(get_class($this).': Chargement de la salle id='.$id);
        return $this->doctrine->getRepository('BackendBundle:Room')->findOneBy(array(
            'id' => $id
        ));
    }

    /**
     * Function to display a room by site
     * @param $id
     * @return array|\Osmose\BackendBundle\Entity\Room[]
     */
    public function getRoomsBySite($id)
    {
        $this->logger->info(get_class($this).': Chargement des salles du site id='.$id);
        $site = $this->doctrine->getRepository("BackendBundle:Site")->findOneBy(array(
            'id'        => $id,
            'enabled'   => true
        ));

        return $this->doctrine->getRepository('BackendBundle:Room')->findBy(array(
            'site' => $site
        ));
    }

    /**
     * @param $id
     * @return array|\Osmose\BackendBundle\Entity\Room[]
     */
    public function disableRoom($id)
    {
        $this->logger->info(get_class($this).': Désactivation de la salle id='.$id);
        $room = $this->getRoom($id);
        $room->setArchived(true);
        try {
            $this->doctrine->persist($room);
            $this->doctrine->flush();
        }catch(\Doctrine\DBAL\DBALException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }catch(\Doctrine\ORM\ORMException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }

        return $room;
    }

    /**
     * @param $id
     * @return array|\Osmose\BackendBundle\Entity\Room[]
     */
    public function enableRoom($id)
    {
        $this->logger->info(get_class($this).': Activation de la salle id='.$id);
        $room = $this->getRoom($id);
        $room->setArchived(false);
        try {
            $this->doctrine->persist($room);
            $this->doctrine->flush();
        }catch(\Doctrine\DBAL\DBALException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }catch(\Doctrine\ORM\ORMException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }

        return $room;
    }

    /**
    * @param $specialityId
    * @return array|\Osmose\BackendBundle\Entity\Room[]
    */
    public function getRoomsBySpeciality($specialityId) {
      $this->logger->info(get_class($this).': Chargement des salles qui ont la spécialité id='.$specialityId);
      $speciality = $this->doctrine->getRepository("BackendBundle:Speciality")->findOneBy(array(
          'id'        => $specialityId
      ));
      if ($speciality->getType() == "GLOBAL") {
        return $this->getAllRooms();
      }else{
        return $this->doctrine->getRepository('BackendBundle:Room')->findBy(array(
          'speciality' => $speciality,
          'archived' => false
        ));
      }

    }

}