<?php
/**
 * Created by PhpStorm.
 * User: Audrophe
 * Date: 03/08/2016
 * Time: 12:52
 */

namespace Osmose\BackendBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class UserService
{

    /**
     * @var EntityManager
     */
    private $doctrine;
    public $logger;
    /**
     * Localisation service constructor.
     *
     * @param EntityManager $doctrine
     */
    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Allow the back to get all the paths.
     *
     * @return array Osmose\UserBundle\Entity\User[]
     */
    public function countUser()
    {
        return $this->doctrine->getRepository('UserBundle:User')->countUsers();
    }

    /**
     * Function to display a room
     * @param $id
     * @return array|\Osmose\UserBundle\Entity\User[]
     */
    public function getUser($id)
    {
        $this->logger->info(get_class($this).': Récupération de l\'utilisateur id='.$id);
        return $this->doctrine->getRepository('UserBundle:User')->findOneBy(array(
            'id' => $id
        ));
    }

    /**
     * Disable User
     * @param User $user
     * @return User
     */
    public function disableUser($user)
    {
        $this->logger->info(get_class($this).': Désactivation du user id='.$user->getId());
        $user->setEnabled(false);
        try {
            $this->doctrine->persist($user);
            $this->doctrine->flush();
        }catch(\Doctrine\DBAL\DBALException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }catch(\Doctrine\ORM\ORMException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }
        return $user;
    }
    /**
     * Enable User
     * @param User $user
     * @return User
     */
    public function enableUser($user)
    {
        $this->logger->info(get_class($this).": Activation de l'utilisateur");
        $user->setEnabled(true);
        try {
            $this->doctrine->persist($user);
            $this->doctrine->flush();
        }catch(\Doctrine\DBAL\DBALException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }catch(\Doctrine\ORM\ORMException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }
        return $user;
    }

    public function getActiveUsers()
    {
        $this->logger->info(get_class($this) . ': Récupération de la liste des utilisateurs actifs');
        return $this->doctrine->getRepository('UserBundle:User')->getActiveUsers();
    }

    /**
     * Function to display all the users
     * @return array|\Osmose\UserBundle\Entity\User[]
     */
    public function getAllUsers()
    {
        $this->logger->info(get_class($this).': Chargement des users');
        return $this->doctrine->getRepository('UserBundle:User')->findAll();
    }

    /**
     * Function to display users by moduleId
     * @return array|\Osmose\UserBundle\Entity\User[]
     */
    public function getUsersByModule($module){
        $this->logger->info(get_class($this).': Chargement des utilisateurs pouvant dispenser le module id='.$module->getId());

        $rsm = new ResultSetMappingBuilder($this->doctrine);
        $rsm->addRootEntityFromClassMetadata('Osmose\UserBundle\Entity\User', 'u');

        $sql = 'SELECT * FROM fos_user u JOIN osmose_user_module um ON u.id = um.user_id WHERE um.module_id = ? AND u.enabled = 1';

        $query = $this->doctrine->createNativeQuery($sql, $rsm);
        $query->setParameter(1, $module->getId());

        try{
            $users = $query->getResult();
        }catch(\Exception $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }

        $this->logger->info(get_class($this).': Sortie de la requête');
        return $users;
    }

    /**
     * Check if the user is available at startsOn EndOn
     * @param $userId
     * @param $startsOn
     * @param $endsOn
     * @return boolean
     */
    public function isUserAvailableAtStartsOnEndsOn($trainer, $start, $end, $updateSlotId){
        $boolResult = true;
        $slots = $this->doctrine->getRepository('BackendBundle:Slot')->findBy(array(
            'trainer' => $trainer
        ));
        foreach ($slots as $slot){
            if (($start < $slot->getStartsOn() && $end > $slot->getStartsOn()) || ($start > $slot->getStartsOn() && $start < $slot->getEndsOn())
                || ($start < $slot->getStartsOn() && $end > $slot->getEndsOn()) || ($start == $slot->getStartsOn())){
                if ($slot->getId() != $updateSlotId){
                    $boolResult = false;
                    break;
                }
            }
        }
        return $boolResult;
    }
}
