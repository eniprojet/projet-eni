<?php
/**
 * Created by PhpStorm.
 * User: Audrophe
 * Date: 03/08/2016
 * Time: 12:52
 */

namespace Osmose\BackendBundle\Services;

use Doctrine\ORM\EntityManager;
use Osmose\BackendBundle\Entity\Site;

class LocalisationService
{
    /**
     * @var EntityManager
     */
    private $doctrine;
    public $logger;

    /**
     * Localisation service constructor.
     *
     * @param EntityManager $doctrine
     */
    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Allow the back to get all the paths.
     *
     * @return array Osmose\BackendBundle\Entity\Site[]
     */
    public function countSites()
    {
        return $this->doctrine->getRepository('BackendBundle:Site')->countSites();
    }

    /**
     * Get all the sites
     *
     * @return array Osmose\BackendBundle\Entity\Site[]
     */
    public function getAllSites()
    {
        $this->logger->info(get_class($this).': Chargement de tout les sites');
        return $this->doctrine->getRepository('BackendBundle:Site')->findAll();
    }

    /**
     * Get all the enabled sites
     *
     * @return array Osmose\BackendBundle\Entity\Site[]
     */
    public function getAllEnabledSites()
    {
        $this->logger->info(get_class($this).': Chargement de tout les sites activés');
        return $this->doctrine->getRepository('BackendBundle:Site')->findBy(array(
            'enabled'   => true
        ));
    }


    /**
     * Get one site
     * @param $id
     * @return Site
     */
    public function getSite($id)
    {
        $this->logger->info(get_class($this).': Chargement du site id='.$id);
        $site = $this->doctrine->getRepository("BackendBundle:Site")->findOneBy(array(
            'id'    => $id,
        ));
        return $site;
    }

    /**
     * Disable Site
     * @param Site $site
     * @return Site
     */
    public function disableLocalisation(Site $site)
    {
        $this->logger->info(get_class($this).': Désactivation du site id='.$site->getId());
        $site->setEnabled(false);
        try {
            $this->doctrine->persist($site);
            $this->doctrine->flush();
        }catch(\Doctrine\DBAL\DBALException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }catch(\Doctrine\ORM\ORMException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }
        return $site;
    }

    /**
     * Disable Site
     * @param Site $site
     * @return Site
     */
    public function enableLocalisation(Site $site)
    {
        $this->logger->info(get_class($this).': Activation du site id='.$site->getId());
        $site->setEnabled(true);
        try {
            $this->doctrine->persist($site);
            $this->doctrine->flush();
        }catch(\Doctrine\DBAL\DBALException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }catch(\Doctrine\ORM\ORMException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }
        return $site;
    }

}