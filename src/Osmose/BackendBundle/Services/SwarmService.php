<?php
/**
 * Created by PhpStorm.
 * User: Joffrey
 * Date: 06/08/16
 * Time: 15:31
 */

namespace Osmose\BackendBundle\Services;

use Doctrine\ORM\EntityManager;

class SwarmService {

    /**
     * @var EntityManager
     */
    private $doctrine;
    public $logger;

    /**
     * Localisation service constructor.
     *
     * @param EntityManager $doctrine
     */
    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Allow the back to get all the paths.
     *
     * @return array Osmose\BackendBundle\Entity\Swarm[]
     */
    public function countSwarms()
    {
        return $this->doctrine->getRepository('BackendBundle:Swarm')->countSwarms();
    }

    /**
     * getAllSwarms
     *
     * @return array Osmose\BackendBundle\Entity\Swarm[]
     */
    public function getAllSwarms()
    {
        $this->logger->info(get_class($this).': Chargement des groupes');
        return $this->doctrine->getRepository('BackendBundle:Swarm')->getAllSwarms();
    }

    /**
     * Function to display a room
     * @param $id
     * @return array|\Osmose\BackendBundle\Entity\Swarm[]
     */
    public function getSwarm($id)
    {
        $this->logger->info(get_class($this).': Chargement du groupe id='.$id);
        return $this->doctrine->getRepository('BackendBundle:Swarm')->findOneBy(array(
            'id' => $id
        ));
    }
}
