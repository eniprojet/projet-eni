<?php
/**
 * Created by PhpStorm.
 * User: Audrophe
 * Date: 03/08/2016
 * Time: 12:52
 */

namespace Osmose\BackendBundle\Services;

use Doctrine\ORM\EntityManager;

class ModuleService
{

    /**
     * @var EntityManager
     */
    private $doctrine;
    public $logger;

    /**
     * Localisation service constructor.
     *
     * @param EntityManager $doctrine
     */
    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Function to display all the Modules
     * @return array|\Osmose\BackendBundle\Entity\Module[]
     */
    public function getAllModules()
    {
        $this->logger->info(get_class($this).': Chargements des modules');
        return $this->doctrine->getRepository('BackendBundle:Module')->getActiveModules();
    }

    /**
     * Function to display a Module
     * @param $id
     * @return array|\Osmose\BackendBundle\Entity\Module[]
     */
    public function getModule($id)
    {
        $this->logger->info(get_class($this).': Chargement du module id='.$id);
        return $this->doctrine->getRepository('BackendBundle:Module')->findOneBy(array(
            'id' => $id
        ));
    }

    /**
     * @param $id
     * @return array|\Osmose\BackendBundle\Entity\Module[]
     */
    public function disableModule($id)
    {
        $this->logger->info(get_class($this).': Désactivation du module id='.$id);
        $module = $this->getModule($id);
        $module->setArchiver(true);
        try {
            $this->doctrine->persist($module);
            $this->doctrine->flush();
        }catch(\Doctrine\DBAL\DBALException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }catch(\Doctrine\ORM\ORMException $e) {
            $this->logger->error(get_class($this).': '.$e->getMessage());
            $this->logger->error(get_class($this).': '.$e->getTraceAsString());
        }
        return $module;
    }

}
