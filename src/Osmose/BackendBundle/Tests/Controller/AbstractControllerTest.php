<?php
namespace Osmose\BackendBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;

abstract class AbstractControllerTest extends WebTestCase
{
    protected $client = null;
    protected $userManager = null;
    protected $testUser = null;


    public function setUp()
    {
        $this->client = $this->createAuthorizedClient();
    }

    protected function createAuthorizedClient()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $session = $container->get('session');
        $this->userManager = $container->get('fos_user.user_manager');
        $loginManager = $container->get('fos_user.security.login_manager');
        $firewallName = $container->getParameter('fos_user.firewall_name');

		$this->userManager->deleteUser($this->userManager->findUserByUsername('test'));
        $this->testUser = $this->userManager->createUser();
        $this->testUser->setUsername('test');
        $this->testUser->setEmail('test@mail.org');
        $this->testUser->setPlainPassword('test');
        $this->userManager->updateUser($this->testUser);

        $user = $this->userManager->findUserBy(array('username' => 'test'));
        $loginManager->loginUser($firewallName, $user);

        $container->get('session')->set('_security_' . $firewallName,
            serialize($container->get('security.context')->getToken()));
        $container->get('session')->save();
        $client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));

        return $client;
    }
}
