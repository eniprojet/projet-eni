<?php

namespace Osmose\BackendBundle\Tests\Controller;

use Osmose\BackendBundle\Tests\Controller\AbstractControllerTest;

class SiteControllerTest extends AbstractControllerTest
{
    public function testIndex()
    {
        $crawler = $this->client->request('GET', '/site/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /site/");
    }

    public function testCreate()
    {
        $crawler = $this->client->request('POST', '/site/', array(
            "osmose_backendbundle_site[name]" => "name",
            "osmose_backendbundle_site[openingHour][hour]" => "0",
            "osmose_backendbundle_site[openingHour][minute]" => "6",
            "osmose_backendbundle_site[closingHour][hour]" => "0",
            "osmose_backendbundle_site[closingHour][minute]" => "8",
            "osmose_backendbundle_site[breakStart][hour]" => "0",
            "osmose_backendbundle_site[breakStart][minute]" => "7",
            "osmose_backendbundle_site[breakEnd][hour]" => "0",
            "osmose_backendbundle_site[breakEnd][minute]" => "8",
            "osmose_backendbundle_site[city]" => "city",
            "osmose_backendbundle_site[address]" => "addr",
        ));

        $crawler = $this->client->request('GET', '/site/');
        $this->assertGreaterThan(0, $crawler->filter('tr')->count(), 'Pas de sites.');
    }
}
