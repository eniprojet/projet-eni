<?php

namespace Osmose\BackendBundle\Tests\Controller;

use Osmose\BackendBundle\Tests\Controller\AbstractControllerTest;

class DefaultControllerTest extends AbstractControllerTest
{
    public function test302IfNotLoggedIn()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertEquals(302, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /formation/");
    }

    public function test200IfLoggedIn()
    {
        $crawler = $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /formation/");
    }
}
