<?php

namespace Osmose\BackendBundle\Tests\Controller;

use Osmose\BackendBundle\Tests\Controller\AbstractControllerTest;

class FormationControllerTest extends AbstractControllerTest
{
    public function testCreate()
    {
        $crawler = $this->client->request('GET', '/formation/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /formation/");
        $crawler = $this->client->click($crawler->selectLink('Enregistrer une nouvelle formation')->link());

        $form = $crawler->selectButton('Create')->form(array(
            'osmose_backendbundle_formation[codeFormation]'  => 'codeFormation',
            'osmose_backendbundle_formation[libelleLong]'  => 'libelleLong',
            'osmose_backendbundle_formation[tauxHoraire]'  => '1',
            'osmose_backendbundle_formation[dateCreation]'  => '2017-06-12T00:00:00+02:00',
            'osmose_backendbundle_formation[codeTitre]'  => 'codeTitre',
            'osmose_backendbundle_formation[prixPublicEnCours]'  => '1',
            'osmose_backendbundle_formation[heuresCentre]'  => '1',
            'osmose_backendbundle_formation[heuresStage]'  => '1',
            'osmose_backendbundle_formation[semainesCentre]'  => '1',
            'osmose_backendbundle_formation[semainesStage]'  => '1',
            'osmose_backendbundle_formation[dureeEnSemaines]'  => '1',
            'osmose_backendbundle_formation[dateModif]'  => '2017-06-12T00:00:00+02:00',
            'osmose_backendbundle_formation[eCFaPasser]'  => 'eCFaPasser',
        ));

        $this->client->submit($form);

        $crawler = $this->client->request('GET', '/formation/');
        $this->assertGreaterThan(0, $crawler->filter('tr')->count(), 'Pas de formations.');
    }
}
