<?php

namespace Osmose\BackendBundle\Tests\Controller;

use Osmose\BackendBundle\Tests\Controller\AbstractControllerTest;

class SlotControllerTest extends AbstractControllerTest
{
    public function testIndex()
    {
        $crawler = $this->client->request('GET', '/slot/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET index");
    }
}
