<?php

namespace Osmose\BackendBundle\Tests\Controller;

use Osmose\BackendBundle\Tests\Controller\AbstractControllerTest;

class ModuleControllerTest extends AbstractControllerTest
{
    public function testIndex()
    {
        $crawler = $this->client->request('GET', '/module/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET index");
    }

    public function testNew()
    {
        $crawler = $this->client->request('GET', '/module/new');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET new");
    }
}
