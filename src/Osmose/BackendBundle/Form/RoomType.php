<?php

namespace Osmose\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RoomType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('seats')
            ->add('computers')
            ->add('projector')
            ->add('number')
            ->add('computersModels')
            ->add('computersRam')
            ->add('computersDisks')
            ->add('computersCpu')
            ->add('warrantyEnd',DateTimeType::class,array(
                'widget'    => 'single_text',
                'required'  => false
            ))
            ->add('site', 'entity', array(
                'class' => 'BackendBundle:Site',
                'property' => 'name'
            ))
            ->add('speciality', 'entity', array(
                'class' => 'BackendBundle:Speciality',
                'property' => 'labelSpeciality'
            ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Osmose\BackendBundle\Entity\Room'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'osmose_backendbundle_room';
    }
}
