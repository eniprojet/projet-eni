<?php

namespace Osmose\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FormationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codeFormation')
            ->add('libelleLong')
            ->add('libelleCourt')
            ->add('dureeEnHeures')
            ->add('tauxHoraire')
            ->add('codeTitre')
            ->add('prixPublicEnCours')
            ->add('heuresCentre')
            ->add('heuresStage')
            ->add('semainesCentre')
            ->add('semainesStage')
            ->add('dureeEnSemaines')
            ->add('eCFaPasser',ChoiceType::class ,array(
                'choices'  => array(
                    '1' => "Oui",
                    '0' => "Non",
                ),
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Osmose\BackendBundle\Entity\Formation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'osmose_backendbundle_formation';
    }
}
