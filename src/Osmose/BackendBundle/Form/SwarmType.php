<?php

namespace Osmose\BackendBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SwarmType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('supervisor',EntityType::class,array(
                'class' => 'UserBundle:User',
                'property' => 'username',
                'empty_value' => 'Choisissez un référent'
            ))
            ->add('codePromotion')
            ->add('libelle')
            ->add('debut',DateTimeType::class,array(
                'widget' => 'single_text'
            ))
            ->add('fin',DateTimeType::class,array(
                'widget' => 'single_text'
            ))
            ->add('prixPublicAffecte')
            ->add('prixPecAffecte')
            ->add('prixFinanceAffecte')
            ->add('site',EntityType::class,array(
                'class' => 'BackendBundle:Site',
                'property' => 'name',
                'empty_value' => 'Choisissez un site'
            ))
            ->add('formation',EntityType::class,array(
                'class' => 'BackendBundle:Formation',
                'property' => 'libelleCourt',
                'empty_value' => 'Choisissez une formation'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Osmose\BackendBundle\Entity\Swarm'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'osmose_backendbundle_swarm';
    }
}
