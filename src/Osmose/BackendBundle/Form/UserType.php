<?php

namespace Osmose\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('roles', 'choice', array(
                'mapped' => true,
                'expanded' => false,
                'multiple' => true,
                'choices' => array(
                    'ROLE_ADMIN'    => 'Administrateur',
                    'ROLE_FORMATEUR'     => 'Formateur',
                )
            ))
            ->add('site', 'entity', array(
                'class' => 'BackendBundle:Site',
                'property' => 'name'
            ))
        ;
    }

    public function getParent() {
        return 'fos_user_registration';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Osmose\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'osmose_backendbundle_user';
    }
}
