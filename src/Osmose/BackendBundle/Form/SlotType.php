<?php

namespace Osmose\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class SlotType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * @todo mettre des requêtes permettant d'afficher les salles disponibles
         * en finction de l'heure choisie
         */
        $builder
            ->add('startsOn')
            ->add('endsOn')
            ->add('note')
            ->add('room', 'entity', array(
                'class' => 'BackendBundle:Room',
                'property' => function ($room) {

                    return "num : {$room->getNumber()} places : {$room->getSeats()} adapté pour : {$room->getSpeciality()->getLabelSpeciality()}";
                }
            ))
            ->add('trainer', 'entity', array(
                'class' => 'UserBundle:User',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->select('u')
                        ->where('u.enabled = true');
                },
                'property'  => 'firstnameLastname'
            ))
            ->add('swarm', 'entity', array(
                'class' => 'BackendBundle:Swarm',
                'property' => 'libelle'
            ))
            ->add('module', 'entity', array(
                'class' => 'BackendBundle:Module',
                'property' => function ($module) {
                    return "{$module->getLibelleCourt()} - {$module->getLibelle()}";
                  },
                'required'    => true,
                'empty_value'  => 'Divers'
            ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Osmose\BackendBundle\Entity\Slot'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'osmose_backendbundle_slot';
    }
}
