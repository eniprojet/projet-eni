<?php

namespace Osmose\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Osmose\BackendBundle\Entity\Site;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="Osmose\UserBundle\EntityRepository\UserRepository")
 */
class User extends BaseUser
{

    public function __construct()
    {
        parent::__construct();
        $this->modules = new ArrayCollection();
        $this->enabled = true;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname;

    /**
     * @var string
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;


    /**
     * @var site
     *
     * @ORM\ManyToOne(targetEntity="Osmose\BackendBundle\Entity\Site", inversedBy="users")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id")
     */
    private $site;

    /**
     * @ORM\ManyToMany(targetEntity="Osmose\BackendBundle\Entity\Module", cascade={"persist"})
     * @ORM\JoinTable(name="osmose_user_module")
     */
    private $modules;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set site
     *
     * @param integer $site
     *
     * @return User
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Add module
     *
     * @param \Osmose\BackendBundle\Entity\Module $module
     *
     * @return User
     */
    public function addModule(\Osmose\BackendBundle\Entity\Module $module)
    {
        $this->modules[] = $module;

        return $this;
    }

    /**
     * Remove module
     *
     * @param \Osmose\BackendBundle\Entity\Module $module
     */
    public function removeModule(\Osmose\BackendBundle\Entity\Module $module)
    {
        $this->modules->removeElement($module);
    }

    /**
     * Get modules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * Set a Collection of modules
     *
     * @param $modules
     * @return mixed
     */
    public function setModules($modules)
    {
        $this->modules = $modules;
    }

    /**
     * Function used in SlotType to concatenate firstname
     * and lastname
     * @return string
     */
    public function getFirstnameLastname()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
}
