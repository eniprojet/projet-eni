<?php
namespace Osmose\UserBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Controller\ChangePasswordController as BaseController;


class ChangePasswordController extends BaseController
{
    /**
     * Change Password
     */
    public function changePasswordAction(Request $request)
    {
        $logger = $this->get('my_logger');
        $logger->info(get_class($this).': Changement de mot de passe');
        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            $logger = $this->get('my_logger');
            $logger->info(get_class($this).': Accès non autorisé');

            $this->get('session')->getFlashBag()->add('error', "Vous n'avez pas accès à cette section.");
            return $this->redirect($this->generateUrl('user_show'));
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.change_password.form.factory');

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);

            $this->get('session')->getFlashBag()->add('info', "Votre mot de passe a bien été modifié.");
            return $this->redirect($this->generateUrl('planning_home'));
        }

        return $this->render('UserBundle:ChangePassword:changePassword.html.twig', array(
            'form'          => $form->createView(),
            'controller'    => 'user'
        ));
    }
}
