<?php

namespace Osmose\UserBundle\Controller;

use FOS\UserBundle\Controller\ProfileController as BaseController;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends BaseController{

    /**
     * Show the user
     */
    public function showAction()
    {
        $logger = $this->get('my_logger');
        $logger->info(get_class($this).': Affichage profil utilisateur');
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->render('UserBundle:Profile:show.html.twig', array(
            'user'              => $user,
            'controller'        => 'user',
            'title_controller'  => 'Mon profil',
            'title_action'      => 'Détails',
            'action'            => 'all',
        ));
    }

    /**
     * Edit the user
     */
    public function editAction(Request $request)
    {
        $logger = $this->get('my_logger');
        $logger->info(get_class($this).': Edition profil utilisateur');
        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            $logger = $this->get('my_logger');
            $logger->info(get_class($this).': Accès non autorisé');

            $this->get('session')->getFlashBag()->add('error', "Vous n'avez pas accès à cette section.");
            return $this->redirect($this->generateUrl('user_show'));
        }
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.profile.form.factory');

        $form = $formFactory->createForm();
        $form->setData($user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);

            $this->get('session')->getFlashBag()->add('info', "Votre profil a été mis à jour.");
            return $this->redirect($this->generateUrl('planning_home'));
        }

        return $this->render('UserBundle:Profile:edit.html.twig', array(
            'user'              => $user,
            'form'              => $form->createView(),
            'controller'        => 'user',
            'title_controller'  => 'Mon profil',
            'title_action'      => 'Détails',
            'action'            => 'all',
        ));
    }
}
