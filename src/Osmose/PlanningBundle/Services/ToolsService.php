<?php

namespace Osmose\PlanningBundle\Services;
use Symfony\Component\HttpFoundation\Request;

class ToolsService
{
    public $roomService;
    public $userService;
    public $swarmService;
    public $moduleService;
    public $slotService;
    public $logger;

    /**
     * This function convert two list of object
     * into an array_list composed by the twice information
     * @param $sites
     * @param $swarms
     * @return mixed
     */
    public function convertSitesToFullCalendarSitesList($sites,$swarms)
    {
        $result = array();
        foreach ($sites as $site) {
            foreach ($swarms as $swarm) {
                if($swarm->getSite()->getId() == $site->getId()){
                    array_push($result, array(
                        'id'        => $swarm->getId(),
                        'building'  => $site->getName(),
                        'title'     => $swarm->getLibelle()
                    ));
                }
            }
        }
        return $result;
    }

    /**
     * This function convert a list of object
     * into an array_list
     * @param $slots
     * @return array
     */
    public function convertSlotsToFullCalendarEventsList($slots)
    {
        $events = array();
        foreach ($slots as $slot) {

            $result = array(
                'id' => $slot->getId(),
                'resourceId' => $slot->getSwarm()->getId(),
                'start' => $slot->getStartsOn()->format('Y-m-d H:i'),
                'trainerName'=>$slot->getTrainer()->getFirstname().' '.$slot->getTrainer()->getLastname(),
                'end' => $slot->getEndsOn()->format('Y-m-d H:i'),
                'note' => $slot->getNote(),
                'roomId' => $slot->getRoom()->getId(),
                'roomCode' => $slot->getRoom()->getCodeSalle(),
                'trainerId' => $slot->getTrainer()->getId(),
                'swarmLibelle' => $slot->getSwarm()->getLibelle(),
                'siteLibelle' => $slot->getSwarm()->getSite()->getName()
            );

            if ($slot->getModule() != null){
                $result['title'] = $slot->getModule()->getLibelleCourt();
                $result['moduleId'] = $slot->getModule()->getId();
            }else{
                $result['title'] = "Divers : ".$slot->getNote();
                $result['moduleId'] = "0";
            }
            array_push($events, $result);
        }
        return $events;
    }

    /**
     * This function convert a slot object
     * into an array
     * @param $slot
     * @return array
     */
    public function convertSlotToFullCalendarEvent($slot)
    {
        $result =  array(
            'id' => $slot->getId(),
            'resourceId' => $slot->getSwarm()->getId(),
            'start' => $slot->getStartsOn()->format('Y-m-d H:i'),
            'trainerName'=>$slot->getTrainer()->getFirstname().' '.$slot->getTrainer()->getLastname(),
            'end' => $slot->getEndsOn()->format('Y-m-d H:i'),
            'note' => $slot->getNote(),
            'roomId' => $slot->getRoom()->getId(),
            'roomCode' => $slot->getRoom()->getCodeSalle(),
            'trainerId' => $slot->getTrainer()->getId(),
            'swarmLibelle' => $slot->getSwarm()->getLibelle(),
            'siteLibelle' => $slot->getSwarm()->getSite()->getName()
        );

        if ($slot->getModule() != null){
            $result['title'] = $slot->getModule()->getLibelleCourt().' - '.$slot->getModule()->getLibelle();
            $result['moduleId'] = $slot->getModule()->getId();
        }else{
            $result['title'] = "Divers : ".$slot->getNote();
            $result['moduleId'] = "0";
        }

        return $result;
    }

    /**
     * This function push elements bound to the Request
     * into an array
     * @param $request
     * @return array
     */
    public function fillRequestBindingSlot(Request $request)
    {
      return array(
          'startsOn' => new \DateTime($request->get('startsOn')),
          'endsOn' => new \DateTime($request->get('endsOn')),
          'note' => $request->get('note'),
          'room' => $this->roomService->getRoom($request->get('room')),
          'trainer' => $this->userService->getUser($request->get('trainer')),
          'swarm' => $this->swarmService->getSwarm($request->get('swarm')),
          'module' => $this->moduleService->getModule($request->get('module'))


      );
    }

    /**
     * This function convert a list of Room
     * into an array
     * @param $rooms
     * @return array
     */
    public function convertRoomsToRoomArray($rooms){
        $roomsArray = array();

        foreach ($rooms as $room) {
            array_push($roomsArray, array(
                'roomId' => $room->getId(),
                'codeRoom' => $room->getCodeSalle(),
                'seats' => $room->getSeats(),
                'specialityLibelle' => $room->getSpeciality()->getLabelSpeciality()
            ));
        }
        return $roomsArray;
    }

    /**
     * This function convert a list of User
     * into an array
     * @param $users
     * @return array
     */
    public function convertUsersToUserArray($users){
        $usersArray = array();

        foreach ($users as $user) {
            array_push($usersArray, array(
                'trainerId' => $user->getId(),
                'lastName' => $user->getLastname(),
                'firstName' => $user->getFirstname(),
            ));
        }
        return $usersArray;
    }

    /**
     * Check if the room is available at startsOn EndOn
     * @param $slotEntity
     * @return boolean
     */
    public function isRoomAvailableToPlanSlot($slotEntity){
        $this->logger->info(get_class($this).': Vérification si le slot peut être planifié dans cette salle');

        return $this->slotService->isRoomAvailableAtStartsOnEndsOn($slotEntity->getRoom(), $slotEntity->getStartsOn(), $slotEntity->getEndsOn(), $slotEntity->getId());

    }

    /**
     * Check if the user is available at startsOn EndOn
     * @param $slotEntity
     * @return boolean
     */
    public function isUserAvailableToPlanSlot($slotEntity){
        $this->logger->info(get_class($this).': Vérification si le slot peut être planifié pour cet utilisateur');

        return $this->userService->isUserAvailableAtStartsOnEndsOn($slotEntity->getTrainer(), $slotEntity->getStartsOn(), $slotEntity->getEndsOn(), $slotEntity->getId());

    }
}
