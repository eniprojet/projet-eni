<?php

namespace Osmose\PlanningBundle\Controller;

use Osmose\BackendBundle\Entity\Slot;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Osmose\BackendBundle\Form\SlotType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Index controller.
 *
 */
class IndexController extends Controller
{

    /**
     * @Route("/", name="planning_home")
     * @Method({"GET", "POST"})
     */
    public function indexAction()
    {
        $logger = $this->get('my_logger');
        $logger->info(get_class($this).': Accès page planning');
        $nbSites = $this->get('backend.localisation')->countSites();
        $nbRooms = $this->get('backend.room')->countActiveRooms();
        $nbSlots = $this->get('backend.slot')->countSlots();
        $nbUsers = $this->get('backend.user')->countUser();

        $sites = $this->get('backend.localisation')->getAllSites();
        $swarms = $this->get('backend.swarm')->getAllSwarms();
        $slots = $this->getSlotsByUserRole();
        $form = $this->createCreateForm(new Slot());

        $resources = $this->get('planning.tools')->convertSitesToFullCalendarSitesList($sites, $swarms);
        $events = $this->get('planning.tools')->convertSlotsToFullCalendarEventsList($slots);

        $this->get('planning.js_vars')->sitesList = $resources;
        $this->get('planning.js_vars')->eventsList = $events;
        $this->get('planning.js_vars')->adminEditableRights = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') ? true : false;

        return $this->render("PlanningBundle:Index:index.html.twig", array(
            'controller' => 'home',
            'title_action' => "Tableau de bord",
            'nbSites' => $nbSites,
            'nbRooms' => $nbRooms,
            'nbSlots' => $nbSlots,
            'nbUsers' => $nbUsers,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/ajax/create", name="planningSlot_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $logger = $this->get('my_logger');
        $logger->info(get_class($this).': Création d\'une nouvelle session');

        if (!$request->isXmlHttpRequest()) {
            return $this->indexAction();
        }
        //persistance du slots JSON récupéré dans le post (request)
        // rechargement des données slots
        $newSlot = $this->get('planning.tools')->fillRequestBindingSlot($request);


        $slotEntity = new Slot();
        $slotEntity->hydrate($newSlot);

        $roomAvailable = $this->get('planning.tools')->isRoomAvailableToPlanSlot($slotEntity);
        $userAvailable = $this->get('planning.tools')->isUserAvailableToPlanSlot($slotEntity);


        if(!$roomAvailable) {
            if (!$userAvailable){
                return new JsonResponse ("La salle {$slotEntity->getRoom()->getCodeSalle()} ainsi que le formateur {$slotEntity->getTrainer()->getFirstnameLastname()} sont indisponibles sur cette plage horaire", JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            }
            return new JsonResponse ("La salle {$slotEntity->getRoom()->getCodeSalle()} est indisponible sur cette plage horaire", JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }else if(!$userAvailable){
            return new JsonResponse ("Le formateur {$slotEntity->getTrainer()->getFirstnameLastname()} est indisponible sur cette plage horaire", JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }else {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($slotEntity);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->logger->error(get_class($this) . ': ' . $e->getMessage());
                $this->logger->error(get_class($this) . ': ' . $e->getTraceAsString());
            } catch (\Doctrine\ORM\ORMException $e) {
                $this->logger->error(get_class($this) . ': ' . $e->getMessage());
                $this->logger->error(get_class($this) . ': ' . $e->getTraceAsString());
            }
            $slotArray = $this->get('planning.tools')->convertSlotToFullCalendarEvent($slotEntity);
            $slotArray['message'] = "Session créée";
            return new JsonResponse ($slotArray, JsonResponse::HTTP_CREATED);
        }
    }

    /**
     * @Route("/ajax/update", name="planningSlot_update")
     */
    public function updateAction(Request $request)
    {
        $logger = $this->get('my_logger');
        $logger->info(get_class($this).': Mise à jour session id='.$request->get('slotId'));
        if (!$request->isXmlHttpRequest()) {
            return $this->indexAction();
        }
        $em = $this->getDoctrine()->getManager();

        $newSlot = $this->get('planning.tools')->fillRequestBindingSlot($request);

        $slotEntity = $em->getRepository('BackendBundle:Slot')->find($request->get('slotId'));

        if (!$slotEntity){
          return new JsonResponse (array('message' => 'erreur lors de la modification de la session ou la session n\'existe pas',
              'eventId' => $request->get('eventId')), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $slotEntity->hydrate($newSlot);

        $roomAvailable = $this->get('planning.tools')->isRoomAvailableToPlanSlot($slotEntity);
        $userAvailable = $this->get('planning.tools')->isUserAvailableToPlanSlot($slotEntity);

        if(!$roomAvailable) {
            if (!$userAvailable){
                return new JsonResponse ("La salle {$slotEntity->getRoom()->getCodeSalle()} ainsi que le formateur {$slotEntity->getTrainer()->getFirstnameLastname()} sont indisponibles sur cette plage horaire", JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            }
            return new JsonResponse ("La salle {$slotEntity->getRoom()->getCodeSalle()} est indisponible sur cette plage horaire", JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }else if(!$userAvailable){
            return new JsonResponse ("Le formateur {$slotEntity->getTrainer()->getFirstnameLastname()} est indisponible sur cette plage horaire", JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }else {

            try {
                $em->merge($slotEntity);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->logger->error(get_class($this) . ': ' . $e->getMessage());
                $this->logger->error(get_class($this) . ': ' . $e->getTraceAsString());
            } catch (\Doctrine\ORM\ORMException $e) {
                $this->logger->error(get_class($this) . ': ' . $e->getMessage());
                $this->logger->error(get_class($this) . ': ' . $e->getTraceAsString());
            }
            $slotArray = $this->get('planning.tools')->convertSlotToFullCalendarEvent($slotEntity);
            $slotArray['eventId'] = $request->get('eventId');
            $slotArray['message'] = "Session modifiée";
            return new JsonResponse ($slotArray, JsonResponse::HTTP_OK);

        }
    }

    /**
     * @Route("/ajax/delete", name="planningSlot_delete")
     */
    public function deleteAction(Request $request)
    {
        $logger = $this->get('my_logger');
        $logger->info(get_class($this).': Suppression session id='.$request->get('slotId'));
        if (!$request->isXmlHttpRequest()) {
            return $this->indexAction();
        }

        $slot = $this->get('backend.slot')->getSlot($request->get('slotId'));


        if (!$slot) {
          return new JsonResponse (array('message' => "le slot n'existe pas",
              'eventId' => $request->get('eventId')), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $codeRetour = $this->get("backend.slot")->deleteSlot($slot);

        if ($codeRetour == 1){
            return new JsonResponse (array('message' => 'Session supprimée',
                'eventId' => $request->get('eventId')), Response::HTTP_OK);
        }
        return new JsonResponse (array('message' => 'erreur lors de la suppression du slot',
            'eventId' => $request->get('eventId')), Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
    * @Route("/ajax/reloadData", name="planningSlot_reloadData")
    */
    public function reloadDataAction(Request $request)
    {
      $logger = $this->get('my_logger');
      $logger->info(get_class($this).': Rechargement liste salle');
      if (!$request->isXmlHttpRequest()) {
        return $this->indexAction();
      }

      if ($request->get('moduleId') != 0) {
          $module = $this->get('backend.module')->getModule($request->get('moduleId'));

          if (!$module) {
              return new JsonResponse (array('message' => 'le module n\'existe pas'), Response::HTTP_INTERNAL_SERVER_ERROR);
          }

          $rooms = $this->get('backend.room')->getRoomsBySpeciality($module->getSpeciality()->getId());

          $users = $this->get('backend.user')->getUsersByModule($module);

          $roomArray = $this->get('planning.tools')->convertRoomsToRoomArray($rooms);
          $userArray = $this->get('planning.tools')->convertUsersToUserArray($users);
          return new JsonResponse (array('rooms' => $roomArray,
              'users' => $userArray), JsonResponse::HTTP_OK);
      }else{
          $rooms = $this->get('backend.room')->getAllRooms();
          $users = $this->get('backend.user')->getActiveUsers();
          $roomArray = $this->get('planning.tools')->convertRoomsToRoomArray($rooms);
          $userArray = $this->get('planning.tools')->convertUsersToUserArray($users);
          return new JsonResponse (array('rooms' => $roomArray,
              'users' => $userArray),  JsonResponse::HTTP_OK);

      }
    }


    public function getSlotsByUserRole()
    {
        $result = null;

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $result = $this->get('backend.slot')->getAllSlots();
        } else {
            $result = $this->get('backend.slot')->getMySlots($this->getUser());
        }

        return $result;
    }

    /**
     * Creates a form to create a Slot entity.
     *
     * @param Slot $entity The entity
     * @Security("has_role('ROLE_ADMIN')")
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Slot $entity)
    {
        $form = $this->createForm(new SlotType(), $entity);
        return $form;
    }


}
