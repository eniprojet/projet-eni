<?php
/**
 * Created by PhpStorm.
 * User: Joffrey
 * Date: 12/05/16
 * Time: 12:03
 */

namespace Osmose\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Osmose\BackendBundle\Entity\Slot;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadSlotData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface{

    private $privateContainer;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->privateContainer = $container;
    }

    public function load(ObjectManager $manager)
    {
        $room1 = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('BackendBundle:Room')->findOneBy(array(
                'number'   => 105
            ));

        $swarm1 = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('BackendBundle:Swarm')->findOneBy(array(
                'libelle'   => "CP-DEV-34"
            ));

        $swarm2 = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('BackendBundle:Swarm')->findOneBy(array(
                'libelle'   => "CP-DEV-35"
            ));

        $formateurUn = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('UserBundle:User')->findOneBy(array(
                'username'   => "formateur-un"
            ));

        $formateurDeux = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('UserBundle:User')->findOneBy(array(
                'username'   => "formateur-deux"
            ));

        $slot1 = new Slot();
        $slot1->setNote("Anglais");
        $slot1->setRoom($room1);
        $slot1->setSwarm($swarm1);
        $slot1->setTrainer($formateurUn);
        $slot1->setStartsOn(new \DateTime("22-08-2016 09:00:00"));
        $slot1->setEndsOn(new \DateTime("22-08-2016 11:30:00"));

        $slot2 = new Slot();
        $slot2->setNote("Marketing");
        $slot2->setRoom($room1);
        $slot2->setSwarm($swarm1);
        $slot2->setTrainer($formateurUn);
        $slot2->setStartsOn(new \DateTime("22-08-2016 14:30:00"));
        $slot2->setEndsOn(new \DateTime("22-08-2016 17:30:00"));

        $slot3 = new Slot();
        $slot3->setNote("CMMI");
        $slot3->setRoom($room1);
        $slot3->setSwarm($swarm2);
        $slot3->setTrainer($formateurDeux);
        $slot3->setStartsOn(new \DateTime("23-08-2016 09:00:00"));
        $slot3->setEndsOn(new \DateTime("23-08-2016 11:30:00"));

        $manager->persist($slot1);
        $manager->persist($slot2);
        $manager->persist($slot3);

        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }

} 