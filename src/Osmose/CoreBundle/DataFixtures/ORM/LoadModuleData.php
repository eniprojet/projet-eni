<?php
/**
 * Created by PhpStorm.
 * User: Joffrey
 * Date: 12/05/16
 * Time: 12:03
 */

namespace Osmose\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Osmose\BackendBundle\Entity\Module;
use Symfony\Component\Finder\Finder;

class LoadModuleData implements FixtureInterface{


    public function load(ObjectManager $manager)
    {
        $fileName = 'files/07-Module.sql';
        $sql = file_get_contents($fileName);
        $manager->getConnection()->executeUpdate($sql);
        $manager->flush();

        $manager->getConnection()->executeUpdate('update eni_module set speciality_id = (select id FROM osmose_speciality_type WHERE code_type = \'GLOBAL\')');
        $manager->flush();
    }

    public function setContainer( ContainerInterface $container = null )
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 6;
    }

}
