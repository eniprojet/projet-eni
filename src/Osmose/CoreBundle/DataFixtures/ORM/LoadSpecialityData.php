<?php
/**
 * Created by PhpStorm.
 * User: Joffrey
 * Date: 12/05/16
 * Time: 12:03
 */

namespace Osmose\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Osmose\BackendBundle\Entity\Speciality;

class LoadSpecialityData implements FixtureInterface{
    
    
    public function load(ObjectManager $manager)
    {
        
        $spe1 = new Speciality();
        $spe1->setLabelSpeciality("Développement");
        $spe1->setType("DEV");

        $spe2 = new Speciality();
        $spe2->setLabelSpeciality("Réseau");
        $spe2->setType("RES");

        $spe3 = new Speciality();
        $spe3->setLabelSpeciality("Développement & Réseau");
        $spe3->setType("GLOBAL");

        $manager->persist($spe1);
        $manager->persist($spe2);
        $manager->persist($spe3);

        $manager->flush();
    }

    public function getOrder()
    {
        return 6;
    }

} 