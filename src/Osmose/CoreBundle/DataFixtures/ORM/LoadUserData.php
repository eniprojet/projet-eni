<?php

namespace Osmose\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Osmose\BackendBundle\Entity\Room;
use Osmose\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface{

    private $privateContainer;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->privateContainer = $container;
    }

    public function load(ObjectManager $manager)
    {
        // Get our userManager, you must implement `ContainerAwareInterface`
        $userManager = $this->privateContainer->get('fos_user.user_manager');

        $modules = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('BackendBundle:Module')->findAll();

        $module1 = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('BackendBundle:Module')->find(1);

        // Création de notre utilisateur ADMIN
        $userAdmin = $userManager->createUser();
        $userAdmin->setUsername('admin');
        $userAdmin->setFirstname('Mica');
        $userAdmin->setLastname('Doe');
        $userAdmin->setEnabled(true);
        $userAdmin->setEmail('admin@eni-ecole.fr');
        $userAdmin->setPlainPassword('admin');
        $userAdmin->setRoles(array('ROLE_ADMIN'));
        $userAdmin->setModules($modules);

        // Création de notre utilisateur FORMATEUR1
        $formateur1 = $userManager->createUser();
        $formateur1->setUsername('formateur-un');
        $formateur1->setFirstname('John');
        $formateur1->setLastname('Doe');
        $formateur1->setEnabled(true);
        $formateur1->setEmail('formateur-un@eni-ecole.fr');
        $formateur1->setPlainPassword('formateur');
        $formateur1->setRoles(array('ROLE_FORMATEUR'));
        $formateur1->setModules($modules);



        // Création de notre utilisateur FORMATEUR2
        $formateur2 = $userManager->createUser();
        $formateur2->setUsername('formateur-deux');
        $formateur2->setFirstname('Michel');
        $formateur2->setLastname('Doe');
        $formateur2->setEnabled(true);
        $formateur2->setEmail('formateur-deux@eni-ecole.fr');
        $formateur2->setPlainPassword('formateur');
        $formateur2->setRoles(array('ROLE_FORMATEUR'));
        $formateur2->addModule($module1);


        // Update the user
        $userManager->updateUser($userAdmin, true);
        $userManager->updateUser($formateur1, true);
        $userManager->updateUser($formateur2, true);
    }

    public function getOrder()
    {
        return 3;
    }

} 