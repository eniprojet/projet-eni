<?php
/**
 * Created by PhpStorm.
 * User: Joffrey
 * Date: 12/05/16
 * Time: 12:03
 */

namespace Osmose\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Osmose\BackendBundle\Entity\Site;


class LoadSiteData implements FixtureInterface{

    public function load(ObjectManager $manager)
    {
        $site1 = new Site();
        $site1->setName("ENI Ecole Informatique Nantes");
        $site1->setAddress("ZAC du Moulin Neuf 2 B, rue Benjamin Franklin");
        $site1->setCity("nantes");
        $site1->setBreakEnd(new \DateTime("12:30"));
        $site1->setBreakStart(new \DateTime("14:30"));
        $site1->setOpeningHour(new \DateTime("09:30"));
        $site1->setClosingHour(new \DateTime("17:30"));
        $site1->setEnabled(true);

        $site2 = new Site();
        $site2->setName("ENI Ecole Informatique Nantes / Carquefou");
        $site2->setAddress("6 Rue Rose Dieng-Kuntz");
        $site2->setCity("nantes");
        $site2->setBreakEnd(new \DateTime("12:30"));
        $site2->setBreakStart(new \DateTime("14:00"));
        $site2->setOpeningHour(new \DateTime("09:00"));
        $site2->setClosingHour(new \DateTime("17:30"));
        $site2->setEnabled(true);

        $site3 = new Site();
        $site3->setName("ENI Ecole Informatique Niort");
        $site3->setAddress("15 avenue Léo Lagrange - Résidence Les Terrasses de l'Atlantique");
        $site3->setCity("niort");
        $site3->setBreakEnd(new \DateTime("12:30"));
        $site3->setBreakStart(new \DateTime("14:00"));
        $site3->setOpeningHour(new \DateTime("09:00"));
        $site3->setClosingHour(new \DateTime("17:30"));
        $site3->setEnabled(true);

        $site4 = new Site();
        $site4->setName("ENI Ecole Informatique Rennes");
        $site4->setAddress("ZAC de La Conterie - 8 rue Léo Lagrange");
        $site4->setCity("rennes");
        $site4->setBreakEnd(new \DateTime("12:30"));
        $site4->setBreakStart(new \DateTime("14:00"));
        $site4->setOpeningHour(new \DateTime("09:00"));
        $site4->setClosingHour(new \DateTime("17:30"));
        $site4->setEnabled(true);

        $manager->persist($site1);
        $manager->persist($site2);
        $manager->persist($site3);
        $manager->persist($site4);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }

} 