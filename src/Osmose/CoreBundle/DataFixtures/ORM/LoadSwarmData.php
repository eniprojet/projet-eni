<?php
/**
 * Created by PhpStorm.
 * User: Joffrey
 * Date: 12/05/16
 * Time: 12:03
 */

namespace Osmose\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Osmose\BackendBundle\Entity\Swarm;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadSwarmData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface{

    private $privateContainer;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->privateContainer = $container;
    }

    public function load(ObjectManager $manager)
    {
        $saintHerblain = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('BackendBundle:Site')->findOneBy(array(
                'name'   => "ENI Ecole Informatique Nantes"
            ));

        $formateurUn = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('UserBundle:User')->findOneBy(array(
                'username'   => "formateur-un"
            ));

        $formateurDeux = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('UserBundle:User')->findOneBy(array(
                'username'   => "formateur-deux"
            ));

        $swarm1 = new Swarm();
        $swarm1->setLibelle("CP-DEV-34");
        $swarm1->setSite($saintHerblain);
        $swarm1->setSupervisor($formateurUn);
        $swarm1->setCodePromotion("001");
        $swarm1->setDebut(new \DateTime("2015-10-01"));
        $swarm1->setFin(new \DateTime("2016-12-31"));
        $swarm1->setPrixPublicAffecte(200.0);
        $swarm1->setPrixPecAffecte(200.0);
        $swarm1->setPrixFinanceAffecte(200.0);

        $swarm2 = new Swarm();
        $swarm2->setLibelle("CP-DEV-35");
        $swarm2->setSite($saintHerblain);
        $swarm2->setSupervisor($formateurUn);
        $swarm2->setCodePromotion("002");
        $swarm2->setDebut(new \DateTime("2016-10-01"));
        $swarm2->setFin(new \DateTime("2016-10-01"));
        $swarm2->setPrixPublicAffecte(200.0);
        $swarm2->setPrixPecAffecte(200.0);
        $swarm2->setPrixFinanceAffecte(200.0);

        $swarm3 = new Swarm();
        $swarm3->setLibelle("CP-RES-01");
        $swarm3->setSite($saintHerblain);
        $swarm3->setSupervisor($formateurDeux);
        $swarm3->setCodePromotion("003");
        $swarm3->setDebut(new \DateTime("2015-10-01"));
        $swarm3->setFin(new \DateTime("2016-12-31"));
        $swarm3->setPrixPublicAffecte(200.0);
        $swarm3->setPrixPecAffecte(200.0);
        $swarm3->setPrixFinanceAffecte(200.0);

        $manager->persist($swarm1);
        $manager->persist($swarm2);
        $manager->persist($swarm3);

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }

} 