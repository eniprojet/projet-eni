<?php
/**
 * Created by PhpStorm.
 * User: Joffrey
 * Date: 12/05/16
 * Time: 12:03
 */

namespace Osmose\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Osmose\BackendBundle\Entity\Room;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadRoomData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface{

    private $privateContainer;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->privateContainer = $container;
    }

    public function load(ObjectManager $manager)
    {
        $saintHerblain = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('BackendBundle:Site')->findOneBy(array(
               'name'   => "ENI Ecole Informatique Nantes"
            ));

        $carquefou = $this->privateContainer->get('doctrine')
            ->getManager()->getRepository('BackendBundle:Site')->findOneBy(array(
                'name'   => "ENI Ecole Informatique Nantes"
            ));

        $room1 = new Room();
        $room1->setSite($saintHerblain);
        $room1->setSeats(25);
        $room1->setComputers('Dell Optiplex 900');
        $room1->setProjector(true);
        $room1->setNumber(105);
        $room1->setCodeSalle(105);
        $room1->setComputersRam("16GO");
        $room1->setComputersCpu("Intel I5");
        $room1->setNetwork(true);
        $room1->setDevelopment(true);
        $room1->setWarrantyEnd(new \DateTime("21-07-2020"));
        $room1->setComputersModels('Dell Optiplex 900');
        $room1->setComputersDisks('4');
        $room1->setArchived(false);

        $room2 = new Room();
        $room2->setSite($saintHerblain);
        $room2->setSeats(25);
        $room2->setComputers('Dell Optiplex 900');
        $room2->setProjector(true);
        $room2->setNumber(106);
        $room2->setCodeSalle(106);
        $room2->setComputersRam("16GO");
        $room2->setComputersCpu("Intel I5");
        $room2->setNetwork(true);
        $room2->setDevelopment(false);
        $room2->setWarrantyEnd(new \DateTime("21-07-2020"));
        $room2->setComputersModels('Dell Optiplex 900');
        $room2->setComputersDisks('4');
        $room2->setArchived(false);

        $room3 = new Room();
        $room3->setSite($saintHerblain);
        $room3->setSeats(25);
        $room3->setComputers('Dell Optiplex 950');
        $room3->setProjector(true);
        $room3->setNumber(205);
        $room3->setCodeSalle(205);
        $room3->setComputersRam("16GO");
        $room3->setComputersCpu("Intel I5");
        $room3->setNetwork(false);
        $room3->setDevelopment(true);
        $room3->setWarrantyEnd(new \DateTime("21-07-2025"));
        $room3->setComputersModels('Dell Optiplex 950');
        $room3->setComputersDisks('2');
        $room3->setArchived(false);

        $room4  = new Room();
        $room4->setSite($carquefou);
        $room4->setSeats(30);
        $room4->setComputers('Dell Optiplex 850');
        $room4->setProjector(true);
        $room4->setNumber(1);
        $room4->setCodeSalle(1);
        $room4->setComputersRam("16GO");
        $room4->setComputersCpu("Intel I5");
        $room4->setNetwork(false);
        $room4->setDevelopment(true);
        $room4->setWarrantyEnd(new \DateTime("21-07-2019"));
        $room4->setComputersModels('Dell Optiplex 850');
        $room4->setComputersDisks('2');
        $room4->setArchived(false);

        $manager->persist($room1);
        $manager->persist($room2);
        $manager->persist($room3);
        $manager->persist($room4);

        $manager->flush();

        $manager->getConnection()->executeUpdate('update osmose_room set speciality_id = (select id FROM osmose_speciality_type WHERE code_type = \'GLOBAL\')');
        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }

}
