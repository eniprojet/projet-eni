<?php
/**
 * Created by PhpStorm.
 * User: Joffrey
 * Date: 30/06/16
 * Time: 19:51
 */

namespace Osmose\CoreBundle\Entity;


abstract class AbstractOsmoseEntity {

    public function hydrate($donnees)
    {
        foreach ($donnees as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }
} 